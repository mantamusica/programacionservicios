/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kike
 */
public class Cliente {

    /**
     * @param args the command line arguments
     */
    
    
    
    
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            
            //creamos el cliente y le damos un puerto 
            Socket cliente;
            cliente = new Socket("localhost" , 2000);
            
            //Flujo de salida de datos
            OutputStream salidaOS = cliente.getOutputStream();
            DataOutputStream flujoDataOut = new DataOutputStream (salidaOS);
            
            //Flujo de entrada
            InputStream entradaIS = entradaIS = cliente.getInputStream();
            DataInputStream flujoDataIn = new DataInputStream (entradaIS);
            
            String textoReci;
            do{
                System.out.println("Escribe un numero");
                String num_usu = teclado.readLine();
                flujoDataOut.writeUTF(num_usu);
                
                textoReci = flujoDataIn.readUTF();
                System.out.println(textoReci);
            }while(!textoReci.equals("Has acertado"));
            
  
            flujoDataIn.close();
            entradaIS.close();
            flujoDataOut.close();
            salidaOS.close();
            
            cliente.close();
            
            
        } catch (IOException ex) {
            System.out.println("IOECEPTION");
        }
        
        
        
        
    }
    
}
