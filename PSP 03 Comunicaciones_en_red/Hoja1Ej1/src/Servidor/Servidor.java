/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kike
 */
public class Servidor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            
            //Creo el servidor y le doy un puerto
            int puerto = 2000;
            int alea = (int) Math.floor((Math.random()*100)+1);
            System.out.println("El numero aleatorio es: " + alea);
            ServerSocket servidor = new ServerSocket (puerto);
            boolean acierta = false;
            
            //El servidor se pone a la espera de que un cliente entre
            System.out.println ("Servidor espera petición conexión del cliente");
            Socket cliente = servidor.accept(); //Esto hace que reciba la conexion del cliente
            
            //Canal de lectura
            InputStream entradaIS = cliente.getInputStream();
            DataInputStream flujoDataIn = new DataInputStream(entradaIS);

            //Canal de escritura            
            OutputStream salidaOS = cliente.getOutputStream();
            DataOutputStream flujoDataOut = new DataOutputStream (salidaOS);
            
            
            
            while(acierta == false){
                
                //recibimos el numero y lo convertimos a entero
                try{
                    
                String num_usu = flujoDataIn.readUTF();
                int num = Integer.parseInt(num_usu);
                
                System.out.println("Recibido el numero " + num);
                
                //comprobamos si coinciden, si se pasa o se queda corto
                if(alea == num){
                    flujoDataOut.writeUTF("Has acertado");
                    acierta = true;
                }else if(alea > num){
                    flujoDataOut.writeUTF("Es un numero mas alto.");
                }else if (alea < num){
                    flujoDataOut.writeUTF("Es un numero menor");
                }
                    
                }catch(NumberFormatException nfe){
                    flujoDataOut.writeUTF("Has introducido un valor incompatible");
                }
                
                
            };
            
            
            flujoDataOut.close();
            salidaOS.close();
            flujoDataIn.close();
            entradaIS.close();
            
            cliente.close();
            servidor.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
            
            
            
    }
    
}
