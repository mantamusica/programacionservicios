/*
 * Cliente se conecta al host por el puerto especificado.
Crea los flujos de entrada y salida
Escribe número introducido por el teclado en el canal, mientras no acierte.
 */
package tarea1;

/**
 *
 * @author Marina
 */
import java.io.*;
import java.net.*;

class Cliente {

    static final String HOST = "localhost";
    static final int Puerto = 2200;

    public Cliente() {
        String datos = new String();
        String num_cliente = new String();

        // para leer del teclado
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            // Me conecto al puerto
            Socket cliente = new Socket(HOST, Puerto);

            // Creo flujo entrada
            InputStream is = cliente.getInputStream();
            DataInputStream entradaC = new DataInputStream(is);
            //creo flujo salida
            OutputStream os = cliente.getOutputStream();
            DataOutputStream salidaC = new DataOutputStream(os);

            //pedimos número, le leemos, le escribimos en el canal y vemos resultado
            datos = entradaC.readUTF();
            System.out.println(datos);

            do { //termino de pedir cuando reciba mensaje "CORRECTO" del servidor
                System.out.println("Introduce el número:");
                num_cliente = reader.readLine();
                salidaC.writeUTF(num_cliente);
                //recojo mensaje del servidor
                datos = entradaC.readUTF();
                System.out.println(datos);
            } while (!datos.equals("CORRECTO")); //mientras no haya acierto

            // Se cierra la conexión
            cliente.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] arg) {
        new Cliente();
    }
}
