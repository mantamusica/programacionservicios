package tarea1;

/*
 Servidor acepta la conexión del cliente y crea sus flujos de entrada y salida;
 Genera el número aleatorio
 Comprueba si el enviado por el cliente coincide con el aleatorio o no
 /**
 *
 * @author Marina
 */
import java.io.*;
import java.net.*;
import java.util.Random;

class Servidor {

    static final int Puerto = 2200;
    
    public Servidor() {

        try {
            // Inicio el servidor en el puerto
            ServerSocket servidor = new ServerSocket(Puerto);
            System.out.println("Escucho el puerto " + Puerto);

            // Se conecta un cliente
            Socket cliente = servidor.accept(); // Crea objeto
            System.out.println("Cliente conectado");

            // flujo de entrada 
            InputStream is = cliente.getInputStream();
            DataInputStream entradaS = new DataInputStream(is);
            //flujo de salida 
            OutputStream os = cliente.getOutputStream();
            DataOutputStream salidaS = new DataOutputStream(os);

            // Número aleatorio
            Random randomGenerator = new Random();
            int num_secreto = randomGenerator.nextInt(200);

            //escribo en el stream de salida un mensaje con codificación utf
            salidaS.writeUTF("Tienes que adivinar un número del 0 al 100");

            //me preparo para comprobar si el numero que recibo es correcto
            int encontrado = 0;  //controlar acierto o no del número aleatorio
            String num_cliente = new String();

            //mientras cliente no acierte el número, pido otro intento
            while (encontrado == 0) {
                num_cliente = entradaS.readUTF(); //leo del flujo de entrada
                System.out.println("\tEl cliente ha dicho:" + num_cliente);

                if (num_secreto == Integer.parseInt(num_cliente)) {
                    salidaS.writeUTF("CORRECTO"); //escribo en flujo de salida
                    encontrado = 1;
                } else {
                    if (num_secreto > Integer.parseInt(num_cliente)) {
                        salidaS.writeUTF("El número secreto es mayor");
                    } else {
                        salidaS.writeUTF("El número secreto es menor");
                    }
                }
            }
            //escfibo en flujo de salida la palabra FIN porque he terminado
            salidaS.writeUTF("FIN");

            // Se cierra la conexión
            cliente.close();
            System.out.println("Cliente desconectado");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] arg) {
        new Servidor();
    }

}
