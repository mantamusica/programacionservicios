/*
 * SErvidor construye socket en puerto especificado y atiende al cliente
Crea los flujos de entradaS y salidaS
Lee el nombre del fichero que le envía el cliente.
Para leer del canal y escribir en el canal usaremos codificación UTF
Si el fichero existe, lo muestra, sino avisa de que no está en el servidor.
 */
package tarea2;

import java.io.*;
import java.net.*;

/**
 *
 * @author Marina
 */
public class Servidor {

    public int puerto = 1800;
    String linea;

    public Servidor() {
        try {
            //construyo socket del servidor en el puerto especificado
            ServerSocket servidor = new ServerSocket(puerto);
            /*Atiendo la petición de conexión del cliente -> 
             accept() -> devuelve objeto Socket 
             a través del que se establecerá la comunicación con el cliente*/
            Socket cliente;
            cliente = servidor.accept();
            System.out.println("Servidor escucha en el puerto: " + puerto);
            System.out.println("Cliente conectado");
            
            //flujos de entradaS y salidaS del servidor
            DataInputStream entradaS; //flujo de entradaS del canal
            entradaS = new DataInputStream(cliente.getInputStream());
            DataOutputStream salidaS; //flujo salidaS canal
            salidaS = new DataOutputStream(cliente.getOutputStream());
            //el cliente lee del flujo de entrada
            String fichero;
            fichero = new String();
            fichero = entradaS.readUTF(); //leemos nombre fichero del canal de entrada
            System.out.println("Cliente quiere leer el fichero: " + fichero);

            try {
                FileInputStream fistream = new FileInputStream(fichero);
                DataInputStream din = new DataInputStream(fistream);
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(din));

                //leeo linea a linea del fichero
                while ((linea = br.readLine()) != null) {
                    salidaS.writeUTF(linea); //escribo linea a linea en canal salida
                }
                din.close(); //cierro flujo de datos de entrada
                salidaS.writeUTF("EOF"); //escribo fin de fichero
                System.out.println("Fichero enviado");
            } catch (Exception e) {
                salidaS.writeUTF("Error, el fichero no existe");
                System.err.println("Error, el fichero no existe");
            }
            //termino cuando el cliente haya recibido todos los datos
            linea = entradaS.readUTF();
            //cerrar la conexion
            cliente.close();
            System.out.println ("Cliente desconectado");
        } catch (IOException ex) {
            System.err.println("Error de entrada salida. " );
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Servidor();
    }

}
