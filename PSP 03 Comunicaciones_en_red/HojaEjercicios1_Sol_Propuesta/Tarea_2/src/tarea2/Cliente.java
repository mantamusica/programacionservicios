/*
 *El cliente se conecta al puerto especificadoi
 Pide ver el contenido de un fichero del servidor, introduciendo su nombre por teclado.
 */
package tarea2;

import java.io.*;
import java.net.*;

/**
 *
 * @author Marina
 */
public class Cliente {

    static String host = "localhost";
    static int puerto = 1800;

    public Cliente() {
        String fichero = new String();
        String datos = new String();
        try {
            //Constructor: cliente se conecta al puerto
            Socket cliente = new Socket(host, puerto);
            //flujos de entradaC y de salidaC
            DataInputStream entradaC = new DataInputStream(cliente.getInputStream());
            DataOutputStream salidaC = new DataOutputStream(cliente.getOutputStream());

            //pedimos el nombre del fichero para leer
            System.out.println("Nombre del fichero a leer");
            //leo del teclado
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(System.in));
            fichero = br.readLine();
            //escribo el nobmre del fichero en el canal de salidaC
            salidaC.writeUTF(fichero);
            do { //leo datos del canal de entradaC hasta fin de fichero
                datos = entradaC.readUTF();
                if (!datos.equals("EOF")) {
                    System.out.println(datos);
                }
            } while (! datos.equals("EOF")); //termino de leer cuando fin fichero
            salidaC.writeUTF("FIN");
            //cierro la conexion
            cliente.close();
        } catch (IOException ex) {
            System.err.println("Error de entrada salida" + ex.toString());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Cliente();
    }

}
