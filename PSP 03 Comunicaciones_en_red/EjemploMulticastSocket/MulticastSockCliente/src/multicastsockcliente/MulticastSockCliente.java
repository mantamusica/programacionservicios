/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multicastsockcliente;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author solas
 */
public class MulticastSockCliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String host = "225.0.0.1"; //Ip de multicast
        int puerto = 4200; //puerto
        try {
            byte[] buffer = new byte[1024]; //buffer para recoger los datos
            //socket multicast en el puerto especificado
            MulticastSocket socket = new MulticastSocket(puerto);
            //grpo de multicast
            InetAddress grupo = InetAddress.getByName(host);
            //datagrama para los paquetes recibidos
            DatagramPacket paquete = new DatagramPacket(buffer,buffer.length);
            //nos unimos al grupo multicast
            socket.joinGroup(grupo);
            socket.receive(paquete);
            String mensaje= new String (paquete.getData());
            System.out.println ("Recibido el mensaje: " + mensaje);
            socket.leaveGroup(grupo);
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(MulticastSockCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
