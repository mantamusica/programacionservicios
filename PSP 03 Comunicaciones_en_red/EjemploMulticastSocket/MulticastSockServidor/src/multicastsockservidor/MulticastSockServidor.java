/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multicastsockservidor;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author solas
 */
public class MulticastSockServidor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String host = "225.0.0.1"; //Ip de multicast
        int puerto = 4200; //puerto
        try {
            //creo el socket multicast
            MulticastSocket ms = new MulticastSocket ();
            //creo grupo multicast especificando la Ip
            InetAddress grupo = InetAddress.getByName(host);
            //array de bytes que envío
            byte [] buffer = "Hola Mundo".getBytes();
            //datagrama que enviamos por el puerto especificado
            DatagramPacket paquete = new DatagramPacket (buffer, buffer.length, grupo, puerto);
            System.out.println ("Enviando " + buffer.length + " bytes a " + paquete.getAddress());            
            ms.send(paquete);   //envio el datagrama
            ms.close();         //cierro el socket
        } catch (IOException ex) {
            Logger.getLogger(MulticastSockServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
