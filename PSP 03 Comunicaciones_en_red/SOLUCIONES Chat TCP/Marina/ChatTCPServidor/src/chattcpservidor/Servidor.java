/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chattcpservidor;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
/**
 *
 * @author solas
 */
public class Servidor extends JFrame implements ActionListener {
    private static final long serialVersionUID = 1L;
    static ServerSocket servidor;
    static final int PUERTO = 55000; //puerto de escucha
    static int NumConexiones = 0; //contador del número de conexiones 
    static int NumActuales = 0; //contador del número de conexiones activas.
    static int MAX = 10; //número máximo de conexiones que puede haber     
    static Socket almacenSockets [] = new Socket[MAX]; //array para almacenar los socket de los clientes
    static JTextField mensaje = new JTextField("");
    static JTextField mensaje2 = new JTextField("");
    private JScrollPane scrollPanel;
    static JTextArea textArea;
    JButton salir = new JButton ("Salir");
   
    
    //constructor
    public Servidor(){
        super ("Servidor de Chat");
        setLayout (null);
        mensaje.setBounds(10, 10, 400, 30);
        add(mensaje);
        mensaje.setEditable(false);
        mensaje2.setBounds(10, 350, 400, 30);
        add(mensaje2);
        mensaje2.setEditable(false);
        
        textArea = new JTextArea();
        scrollPanel = new JScrollPane(textArea);        
        scrollPanel.setBounds(10, 50, 400, 300);
        add(scrollPanel);
        
        salir.setBounds(450, 10, 100, 30);
        add(salir);
        
        textArea.setEditable(false);
        salir.addActionListener(this);
        setDefaultCloseOperation (JFrame.DO_NOTHING_ON_CLOSE);
        //anulo el cierre de la ventana, cerrará el botón salir
    }
    /*El cierre de la ventana del servidor se hace solo con el botón Salir.
    El botón Salir cierra el SErverSocket y finaliza ejecución servidor*/

    /**
     *
     * @param ae
     */

    @Override
    public void actionPerformed (ActionEvent ae){
        //si he pulsado Salir, cierro servidor
        if (ae.getSource()==salir){
            try {
                servidor.close();
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
           System.exit(0);
        }
    }
    public static void main(String[] args) {
        try {
            servidor = new ServerSocket (PUERTO);
            System.out.println ("Servidor en marcha");
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        Servidor pantalla = new Servidor(); //pantalla del servidor
        pantalla.setBounds(0, 0, 540, 400);
        pantalla.setVisible(true);
        mensaje.setText("Número de conexiones activas: " +0);
        /*
        Controlo el numero de conexiones con blucle.
        Bucle termina si llego al MAX de conexiones permitidas o pulso Salir
        */
        
        while (NumConexiones<MAX){
            //el servidor espera conexion cliente y crea socket cuando se conecta
            Socket socket= new Socket();
            try{
                socket = servidor.accept();//espero al cliente
                
            } catch (SocketException ex) {
                break; //he pulsado botón salir y ya no sigue en el bucle
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
            //almaceno el socket en el array de conexiones
            almacenSockets[NumConexiones]= socket; 
            NumConexiones ++; //aumento el contador de número de conexiones
            NumActuales ++;  //aumento el contador de conexiones acutales
            //lanzo el hilo que gestiona los mensajes del cliente que se ha conectado
            HiloServidor hilo = new HiloServidor(socket);
            hilo.start();
        }
        //compruebo servidor cerrado, si no fue cerrado primero
        if (!servidor.isClosed()){
            mensaje2.setForeground(Color.blue);
            mensaje2.setText("Nº conexiones establecidas: " + NumConexiones);
            try {
                servidor.close();
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    }
}
