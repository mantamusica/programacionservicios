/*
 * Recibe y envia mensajes a los clientes del chat.
Recibe el Socket en el constructor y crea los flujos de I/O para los mensajes.
 */
package chattcpservidor;
import java.io.*;
import java.net.*;
/**
 *
 * @author solas
 */
public class HiloServidor extends Thread{
    DataInputStream entrada; //para flujo entrada
    Socket socket = null;
    public HiloServidor (Socket sc){
        try {
            socket =sc;
            //flujo entrada
            entrada = new DataInputStream (socket.getInputStream());
        } catch (IOException ex) {
            System.out.println ("Error de entrada salida en el hilo" + ex.toString());
        }
    }
    /*run() envia al cliente los mensajes que hay en el chat en ese momento.
    Crearemos el método EnvioMensajes(). Se envian los mensajes que están en 
    el textarea del servidor del chat.
    También recibiremos lo que el cliente escribe en el chat (en un while)
    Cuando el cliente inaliza (botón Salir de la pantalla), se envía una marca
    de fin de comunicación al servidor (en este caso un asterisco) y sale del while.
    Esta es una forma de controlar las conexiones actuales. Cuando salgo del while,
    termino las conexiones actuales*/
    @Override
    public void run(){
        Servidor.mensaje.setText("Num Conexiones ahora: " + Servidor.NumConexiones);
        //envio los mensajes al programa cliente
        String texto = Servidor.textArea.getText();
        EnvioMensajes (texto);
        //recojo mensajes que escribe el cliente
        while (true) {
            String cadena ="";
            String fin ="*";
            
            try{
                cadena = entrada.readUTF(); //leo del flujo de entrada del canal
                System.out.println ("Cadena que leo del canal en run() "+ cadena);
                if (cadena.equals(fin)){ // el cliente se va
                    Servidor.NumActuales--; //un cliente se marcha
                    Servidor.mensaje.setText("NumActuales= " + Servidor.NumActuales);                                 
                    break; //salgo del bucle
                }
                //añado el texto del cliente al textarea del servidor
                //el servidor envia los mensajes del textarea a todos los clientes 
                Servidor.textArea.append(cadena + "\n");
                texto = Servidor.textArea.getText();
                EnvioMensajes (texto);
            } catch (IOException ex) {
                System.err.println("Error IO run del hilo" + ex.toString());
                break;
            }
        }        
    }
    public void EnvioMensajes (String texto){
        int i;
        //recorrer la tabla de sockets para enviar los mensajes
        
        System.out.println ("Estoy en EnvioMensajes, numConexiones = " + Servidor.NumConexiones);
        for (i=0; i<Servidor.NumConexiones; i++){
            Socket s1= Servidor.almacenSockets[i];
            try{
                DataOutputStream salida = new DataOutputStream (s1.getOutputStream());
                salida.writeUTF(texto);
            } catch (SocketException e){
                System.err.println ("Error al intentar escribir el el socket");
            }
            catch (IOException ex) {
                System.err.println ("Error al enviar mensajes" + ex.toString());
            }
        }
    }
}
