/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chattcpcliente;

import java.io.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.*;

/**
 *
 * @author solas
 */
public class Cliente extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;
    Socket socket = null;
    //flujos de entrada y salida
    DataInputStream entrada;
    DataOutputStream salida;
    static String nombre;
    static JTextField mensaje = new JTextField();
    private final JScrollPane scrollpanel;
    static JTextArea textarea1;
    JButton botonEnviar = new JButton("Enviar");
    JButton botonDesconectar = new JButton("Salir");
    boolean repetir = true;

    //Constructor
    public Cliente(Socket sc, String nom) {
        super("Cliente que se conecta = " + nom);
        setLayout(null);
        mensaje.setBounds(10, 10, 400, 30);
        add(mensaje);
        textarea1 = new JTextArea();
        scrollpanel = new JScrollPane(textarea1);
        scrollpanel.setBounds(10, 50, 400, 300);
        add(scrollpanel);
        botonEnviar.setBounds(420, 10, 100, 30);
        add(botonEnviar);
        botonDesconectar.setBounds(420, 50, 100, 30);
        add(botonDesconectar);
        textarea1.setEditable(false);
        botonEnviar.addActionListener(this);
        botonDesconectar.addActionListener(this);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        socket = sc;
        this.nombre = nom;
        //creo flujos entrada y salida
        try {
            entrada = new DataInputStream(socket.getInputStream());
            salida = new DataOutputStream(socket.getOutputStream());
            String texto = " >Entra en el chat: " + nombre;
            salida.writeUTF(texto);
        } catch (IOException ex) {
            System.err.println("Error de entrada/salida" + ex.toString());
            System.exit(0);
        }
    }

    //Si pulso el botonEnviar Enviar se envia el flujo de salida
    @Override
    public void actionPerformed(ActionEvent ae) {
        //si pulso botonSalir me voy
        if (ae.getSource() == botonDesconectar) {
            System.out.println ("He pulsado desconectar");
            String texto = " > " + nombre + " Sale del chat";
            try {
                salida.writeUTF(texto);
                salida.writeUTF("*");
                repetir = false;
                
            } catch (IOException ex) {
                System.err.println("Error al desconectar el cliente" + ex.toString());
            }
        }
        //si pulso botonEnviar Enviar envio el texto
        if (ae.getSource() == botonEnviar) {
            String texto = nombre + "> " + mensaje.getText();
            try {
                mensaje.setText(""); //limpio area de entrada de texto
                salida.writeUTF(texto);
            } catch (IOException ex) {
                System.err.println("Error cliente al enviar el mensaje" + ex.toString());
            }
        }

    }


    /*
    El cliente lee lo que recibe eld hilo (los mensajes del chat) y los muestra en el textarea1
    SE hace esto repetitivamente hasta que el cliente sale
  
     */
    public void ejecutar() {
        String texto = "";
        while (repetir) {
            try {
                texto = entrada.readUTF(); //leo mensajes de stream de entrada
                textarea1.setText(texto); //veo los mensajes
            } catch (IOException ex) {//error si el servidor se cierra
                JOptionPane.showMessageDialog(null, "Imposible conectar con servidor");
                System.err.println(ex.toString() + JOptionPane.ERROR_MESSAGE);
                repetir = false; //sale del bucle
            }
        }
        try {
            System.out.println ("He salido del while en ejecutar()");
            socket.close();
            System.exit(0);
        } catch (IOException ex) {
            System.err.println(ex.toString());
        }
    }

    /**
     * Programa principal pide el nombre del usuario y se conecta con servidro
     * Crea objeto cliente,se muestra en pantalla y se llama a ejecutar()
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int puerto = 55000;
        String nick = JOptionPane.showInputDialog("Escribe tu nick");
        Socket socket = null;
        try {
            //cliente y servidor ejecutandose en localhos
            socket = new Socket("localhost", puerto);

        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Imposible conectar con servidor"
                    + ex.getMessage(), "Mensaje de error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        if (!nick.trim().equals("")) {
            Cliente cliente = new Cliente(socket, nick);
            cliente.setBounds(0, 0, 540, 400);
            cliente.setVisible(true);
            cliente.ejecutar();
        } else {
            System.out.println("Es obligatorio identificarse con un nick o nombre");
        }
    }

}
