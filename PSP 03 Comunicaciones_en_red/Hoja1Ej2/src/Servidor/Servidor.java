/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servidor;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kike
 */
public class Servidor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            // TODO code application logic here
            //creamos el servidor, le damos un puerto, creamos un cliente = servodpr. accept
            int puerto = 1500;
            ServerSocket servidor = new ServerSocket (puerto);
            System.out.println("Servidor a la espera de un cliente");
            Socket cliente = servidor.accept();
            
            
            //creamos el fichero que contiene el texto
            File fich = new File("C:\\letra.txt");
            
            //flujo de escritura del servidor
            OutputStream os = cliente.getOutputStream();
            DataOutputStream flujoOut = new DataOutputStream(os);
            
            //flujo de lectura del fichero
            FileReader fr = new FileReader(fich);
            BufferedReader bfr = new BufferedReader(fr);
            
            String lee = bfr.readLine();
            
            while(lee != null){
                
                flujoOut.writeUTF(lee);
                lee = bfr.readLine();
            }
            
            bfr.close();
            fr.close();
            flujoOut.close();
            os.close();
            
            cliente.close();
            servidor.close();
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }   
           
        
         
    }
    
}
