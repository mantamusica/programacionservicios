/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorobj;
import java.io.*;
/**
 *
 * @author solas
 */
public class Persona implements Serializable{
    String nombre;
    int edad;
    public Persona (String n, int e){
        super();
        this.nombre=n;
        this.edad= e;
    }
    public Persona () {
        super();
    }
    public String getNombre () {
        return nombre;
    }   
    public void setNombre (String name){
        this.nombre = name;
    }
    public int getEdad(){
        return edad;
    }
    public void setEdad(int ed){
        this.edad=ed;
    }
}
