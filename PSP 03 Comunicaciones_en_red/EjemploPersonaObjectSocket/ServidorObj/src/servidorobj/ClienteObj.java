/*
 * El cliente recibe los datos del objeto persona. Los modifica y los devuelve
 */
package servidorobj;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author solas
 */
public class ClienteObj {

    public static void main(String[] args) {

        try {
            String host = "localhost";
            int puerto = 6600;
            System.out.println("Cliente");
            Socket cliente = new Socket(host, puerto);
            //flujo de entrada para leer los datos
            ObjectInputStream inObjeto = new ObjectInputStream(cliente.getInputStream());
            //Leo los datos de la persona enviados por el servidor
            Persona datos = (Persona) inObjeto.readObject();
            System.out.println("Recibo del Servidor los datos de la Persona = "
                    + datos.getNombre() + " edad " + datos.getEdad());
            //Modifico los datos del objeto persona
            datos.setNombre("Fernando");
            datos.setEdad(20);

            //Flujo de salida para escribir los datos en el stream
            ObjectOutputStream outObjeto = new ObjectOutputStream(
                    cliente.getOutputStream());
            //envio los nuevos datos al servidor
            outObjeto.writeObject(datos);
            System.out.println("Enviados los datos:" + datos.getNombre()
                    + " edad " + datos.getEdad());
            //cerramos flujos y socket
            inObjeto.close();
            outObjeto.close();
            cliente.close();
            
        } catch (IOException ex) {
            Logger.getLogger(ClienteObj.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ClienteObj.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
