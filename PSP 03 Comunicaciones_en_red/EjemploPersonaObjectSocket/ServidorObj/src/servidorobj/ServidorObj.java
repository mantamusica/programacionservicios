/*
 * SErvidor: crea un objeto de tipo persona u lo envía al cliente
El cliente puede cambiar los datos del objeto y enviarlos al servidor.
 */
package servidorobj;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author solas
 */
public class ServidorObj {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            int puerto = 6600;
            ServerSocket servidor = new ServerSocket(puerto);
            System.out.println("Servidor esperando al cliente");
            Socket cliente = servidor.accept();
            //Se prepara un flujo de salida para escribir objetos en stream
            ObjectOutputStream outObjeto = new ObjectOutputStream(
                    cliente.getOutputStream());
            //Objeto persona que quiero enviar:
            Persona persona = new Persona("Marina", 40);
            //envio el objeto persona por el stream
            outObjeto.writeObject(persona);
            System.out.println("Enviando los siguientes datos de la persona: "
                    + persona.getNombre() + " edad = " + persona.getEdad());

            /*Cuando el cliente  haya modificado los datos de la persona, los
            voy a leer del stream. Así que lo preparo*/
            ObjectInputStream inObjeto = new ObjectInputStream(
                    cliente.getInputStream()); //para leer objetos del stream
            Persona datosNuevos = (Persona) inObjeto.readObject(); //arroja ClassNotFoundException
            System.out.println ("Los nuevos datos de la persona son: "
            + datosNuevos.getNombre() + " edad " + datosNuevos.getEdad());
            //Cierro Stream y socket
            outObjeto.close();
            inObjeto.close();
            cliente.close();
            servidor.close();
        } catch (IOException ex) {
            Logger.getLogger(ServidorObj.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServidorObj.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
