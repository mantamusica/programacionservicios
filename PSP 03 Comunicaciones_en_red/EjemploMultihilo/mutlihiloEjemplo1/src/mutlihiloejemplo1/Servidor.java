/*
 * El servidor:
 * Crea ServerSocket en el puerto 6666.
 * Acepta las peticiones de conexion de los sockets clientes (a la escucha).
 * Inicia el hilo de ejecución de cada socket
 */
package mutlihiloejemplo1;
import java.io.*;
import java.net.*;
/**
 *
 * @author solas
 */
public class Servidor {    
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        ServerSocket servidor = new ServerSocket(6666);
        System.out.println ("Servidor preparado para trabajar");
        while (true){ //bucle infinito
            Socket cliente = new Socket();
            cliente = servidor.accept();
            HiloDelServidor hilo = new HiloDelServidor (cliente);
            hilo.start();
        }
    }    
}
