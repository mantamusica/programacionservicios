/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteftp;

//librerias de Apache para FTP
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

//librerias de JAVA
import java.io.*;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Clase para recuperar fichero de un servidor FTP.
 * El fichero se deposita en el primer nivel de la carpeta del proyecto.
 * 
 * OJO!!! Es posible que haya que deshabilitar temporalmente el firewall
 *
 * @author msr
 */
//public class Main {
//    //vamos a descargar el archivo README de ftp.rediris.es/pub/FreeBSD/doc/
//    //objeto de la clase FTPCliente de Apache-> métodos para bajar archivo del servidor FTP
//    private static FTPClient clienteFTP;
//    //flujo de salida para escribir datos en un fichero
//    private static FileOutputStream ficheroBajado;
//    //URL del servidor ftp
//    private static final String urlServidor = "ftp.rediris.es";    
//    //ruta del serivdor ftp
//    private static final String rutaFichero = "pub/FreeBSD/doc";
//    //nombre dle fichero de texto que voy a descargar
//    private static final String nombreFichero ="README";
//    //usuario
//    private static final String usuario = "anonymous";
//    private static final String password ="";
//    private static String[] nombreCarpeta;
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args) {
//        try {
//            int reply;
//            clienteFTP = new FTPClient(); //cliente ftp
//            clienteFTP.connect(urlServidor); //conexion al servidor FTP
//            //comprobar la conexion
//            reply = clienteFTP.getReplyCode();
//            //si la conexión se produce
//            if (FTPReply.isPositiveCompletion (reply)){
//                //abro la sesion
//                clienteFTP.login (usuario, password);
//                System.out.println ("Carpetas disponibles en el servidor: ");
//                nombreCarpeta = clienteFTP.listNames();
//                for (int i=0; i<nombreCarpeta.length; i++){
//                    System.out.println (nombreCarpeta[i]);
//                }
//                //fichero que quiero bajar
//                try{
//                    ficheroBajado =new FileOutputStream(nombreFichero);
//                    System.out.println ("Bajadno fichero " + nombreFichero + " de " + rutaFichero);
//                    //escribo contenido del fichero en un nuevo fichero
//                    clienteFTP.retrieveFile ("/" + rutaFichero + "/" +
//                            nombreFichero, ficheroBajado);
//                    //cierro nuevo fichero
//                    ficheroBajado.close();
//                    //cierro conexion
//                    clienteFTP.disconnect();
//                    System.out.println ("**** Descarga finalizada ******");
//                } catch (FileNotFoundException ex) {
//                    System.err.println ("Fichero no encontrado en el servidor");
//                } catch (IOException ex) {
//                    System.err.println ("Error al cerrar el fichero bajado");
//                }
//            } else {
//                clienteFTP.disconnect(); //desconecta
//                System.err.println ("FTP - conexión rechazada");
//                System.exit(-1);
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//    }
//    
//    
//    
//}
public class Main {

  //objeto de la clase FTPClient de Apache, con diversos métodos para
  //interactuar y recuperar un archivo de un servidor FTP
  private static FTPClient clienteFTP;
  //flujo de salida para la escritura de datos en un fichero
  private static FileOutputStream ficheroObtenido;
  //URL del servidor
  private static String servidorURL = "ftp.rediris.es";
  //ruta relativa (en Servidor FTP) de la carpeta que contiene
  //el fichero que vamos a descargar
  private static String rutaFichero = "debian";
  //nombre del fichero (aunque carece de extensión, se trata de un fichero de
  //texto que puede abrise con el bloc de notas)
  private static String nombreFichero = "README";
  //usuario
  private static String usuario = "anonymous";
  //contraseña
  private static String password = "";
  //array de carpetas disponibles
  private static String[] nombreCarpeta;

  /**
   * **************************************************************************
   * recupera el contenido de un fichero desde un Servidor FTP, y lo deposita en
   * un nuevo fichero en el directorio de nuestro proyecto
   *
   * @param args
   */
  public static void main(String[] args) {
    try {
      int reply;
      //creación del objeto cliente FTP
      clienteFTP = new FTPClient();
      //conexión del cliente al servidor FTP
      clienteFTP.connect("ftp.rediris.es");
      //omprobación de la conexión
      reply = clienteFTP.getReplyCode();
      //si la conexión  es satisfactoria
      if (FTPReply.isPositiveCompletion(reply)) {
        //abre una sesión con el usuario anónimo
        clienteFTP.login(usuario, password);
        //lista las carpetas de primer nivel del servidor FTP
        System.out.println("Carpetas disponibles en el Servidor:");
        nombreCarpeta = clienteFTP.listNames();
        for (int i = 0; i < nombreCarpeta.length; i++) {
          System.out.println(nombreCarpeta[i]);
        }
        //nombre que el que va a recuperarse
        ficheroObtenido = new FileOutputStream(nombreFichero);
        //mensaje
        System.out.println("\nDescargando el fichero " + nombreFichero + " de "
                + "la carpeta " + rutaFichero);
        //recupera el contenido del fichero en el Servidor, y lo escribe en el
        //nuevo fichero del directorio del proyecto
        clienteFTP.retrieveFile("/" + rutaFichero + "/"
               + nombreFichero, ficheroObtenido);
        //cierra el nuevo fichero
        ficheroObtenido.close();
        //cierra la conexión con el Servidor
        clienteFTP.disconnect();
        //
        System.out.println("Descarga finalizada correctamente");
      } else {
        //desconecta
        clienteFTP.disconnect();
        System.err.println("FTP ha rechazado la conexión esblecida");
        System.exit(1);
      }
    } catch (SocketException ex) {
      //error de Socket
      System.out.println(ex.toString());
    } catch (IOException ex) {
      //error de fichero
      System.out.println(ex.toString());
    }
  }
}