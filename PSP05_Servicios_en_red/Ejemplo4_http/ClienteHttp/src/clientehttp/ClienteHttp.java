/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientehttp;
import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *Ejemplo cliente realizado con sockets
 * 
 * @author msr
 */
public class ClienteHttp {
    private static String SERVER= "aglinformatica.es";
    private static int PUERTO = 6080;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       try{
           Socket cliente = new Socket (SERVER, PUERTO);
           InputStream is = cliente.getInputStream();
           BufferedReader br = new BufferedReader (new InputStreamReader (is));
           PrintWriter printWriter = new PrintWriter(cliente.getOutputStream(), true);
           //envío comando GET al servidor
           printWriter.println ("GET/index.html");
           //leer linea a linea
           String linea= null;
           while ((linea=br.readLine())!=null){
               System.out.println (linea);
           }
           //cierro socket
           cliente.close();
       } catch (IOException ex) { 
            Logger.getLogger(ClienteHttp.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
}
