/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplofile0;
import java.io.*;

/**
 *
 * @author Marina
 */
public class EjemploFile0 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
   String nombreDir; //Recoge el nombre del directorio pasado
   File directorio; //Registra el archivo del dir pasado
   String[] listanombres; //Lista para guardarcon los nombres de los ficheros
   
   if (args.length==0){
       System.out.println ("Error. Debes llamar al programa pasando el nombre de un directorio");
       System.exit(-1);
   }
    nombreDir = args[0];
    //Preparo acceso a dir
    directorio = new File(nombreDir);
    //Valido si el directorio existe o no
    if (!directorio.exists()){
        System.out.println ("El directorio pasado no existe");
        System.exit(-1);
    }
    //valido si se trata de un directorio o no
    if (!directorio.isDirectory()){
        System.out.println ("El nombre de fichero " + args[0] +" no es directorio");
        System.exit(-1);
    }
    //obtengo lista de nombres de ficheros en el directorio
    listanombres=directorio.list();
    System.out.println("Directorio " + args[0]);
    for (int i=0; i<listanombres.length; ++i){
        System.out.println (listanombres[i]);
    }
    }
    
}
