/*
 * Crear socket cliente y lo conecta al puerto 6000 en el host local 
(es necesario que haya un ServerSocket escuchando en ese puerto).
Visualiza el puerto local desde el que se ha conectado el socket y el puerto,
host e IP de la máquina remota a la que se conecta (en este caso el host local).
 */
package ejemplo4_socket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            String host = "localhost"; // host remoto
            int puerto = 6000; // puerto remoto
            ServerSocket servidor = new ServerSocket (puerto);
            //Crea un socket asociado al Puerto y nombre de host indicados
            Socket cliente = new Socket (host,puerto); //creo cliente
            InetAddress ia = cliente.getInetAddress(); //obtengo la ip del cliente
            
            //visualizamos puertos e IPs locales y remotos
            System.out.println ("Puerto local " + cliente.getLocalPort());
            System.out.println ("Puerto remoto " + cliente.getPort());
            System.out.println ("Host remoto " + ia.getHostName());
            System.out.println ("IP host remoto " + ia.getHostAddress());
            
            //cierro socket
            cliente.close();
            
        } catch (IOException ex) {
            System.err.println ("Error de entrada salida");
            System.err.println (ex.toString());
        }
    }
    
}
