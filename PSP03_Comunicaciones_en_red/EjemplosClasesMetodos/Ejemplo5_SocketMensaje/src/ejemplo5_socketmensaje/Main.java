/*
 * Servidor recibe mensaje del cliente y lo envía por pantalla. 
 Después envía un mensaje al cliente..
 */
package ejemplo5_socketmensaje;

import java.io.*;
import java.net.*;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            int puerto = 6000;
            ServerSocket servidor = new ServerSocket(puerto);
            Socket cliente = null;
            System.out.println("Servidor espera petición conexión del cliente");
            cliente = servidor.accept();

            /**
             * *****Creo flujo de entrada del cliente***** InputStream entradaIS
             * = cliente.getInputStream(); DataInputStream flujoDataIn = new
             * DataInputStream (entradaIS);
             *
             * //El cliente envía mensaje. //leo bytes desde el inputstream
             * System.out.println ("Recibo mensaje del cliente :\n" +
             * flujoDataIn.readUTF());
             */
            /**
             * * Creo flujo de salida al cliente ***********
             */
            OutputStream salidaOS = cliente.getOutputStream();
            DataOutputStream flujoDataOut = new DataOutputStream(salidaOS);

            //Servidor envia saludo al cliente
            flujoDataOut.writeUTF("Saludos del servidor MaQuiNa Puerto: "+cliente.getLocalPort());
            InputStream bufferEntrada = cliente.getInputStream();
            DataInputStream datos = new DataInputStream(bufferEntrada);
            System.out.println(datos.readUTF());

            //Cierro streams y sockets
            //entradaIS.close();
            //flujoDataIn.close();
            salidaOS.close();
            flujoDataOut.close();
            bufferEntrada.close();
            datos.close();
            cliente.close();
        } catch (IOException ex) {
            System.err.println("Error de entrada salida");
            System.err.println(ex.toString());
        }
    }

}
