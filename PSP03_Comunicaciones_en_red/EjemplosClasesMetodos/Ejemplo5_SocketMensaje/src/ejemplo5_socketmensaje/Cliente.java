/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo5_socketmensaje;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author MaQuiNa1995
 */
public class Cliente {

    
    public static void main(String[] args) {

                try {
            Socket canalComunicacion = null;
            
            OutputStream bufferSalida;
            InputStream bufferEntrada;
            DataOutputStream datos;
            DataInputStream datosin;
            
            //String puerto="192.168.37.212";
            String puerto="127.0.0.1";
                    
            canalComunicacion = new Socket(puerto, 6000);
            bufferSalida = canalComunicacion.getOutputStream();
            bufferEntrada = canalComunicacion.getInputStream();
            datos = new DataOutputStream(bufferSalida);
            
            datosin = new DataInputStream(bufferEntrada);
            System.out.println(datosin.readUTF());
            datosin.close();
            datos.close();
            bufferSalida.close();
            bufferEntrada.close();
            
            canalComunicacion.close();
        } catch (UnknownHostException ex) {
           ex.printStackTrace();
        } catch (IOException ex) {
           ex.printStackTrace();
        }
    }
    
}
