/*
 * Crea socket en servidor y lo enlaza con puerto 6000, 
visualiza elpuerto por el qu espera conexiones y espera se conecten 2 clientes.
 */
package ejemplo3_serversocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            int puerto = 6000;
            ServerSocket servidor = new ServerSocket (puerto);
            
            System.out.println ("Servidor escucha en puerto = " +servidor.getLocalPort());
            
            Socket cliente1 = servidor.accept(); //esperar conexión cliente 1
            Socket cliente2 = servidor.accept(); //esperar conexión cliente 2
            
            
            
            /****** instrucciones con las acciones de los clientes *******/
            
            servidor.close();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
