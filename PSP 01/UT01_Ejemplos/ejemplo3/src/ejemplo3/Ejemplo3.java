/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo3;
import java.io.*;

/**
 *
 * @author Marina
 */
public class Ejemplo3 {

    public static void main(String[] args) throws IOException {
        Runtime rt = Runtime.getRuntime();
        Process p = null;
        String comando = "CMD /C VERS"; 
        InputStream is; //Stream para la entrada      
        BufferedReader bfr; 
        try {
            p = rt.exec(comando);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        is = p.getInputStream();      
        bfr = new BufferedReader(new InputStreamReader (is));
        try {
            String line = bfr.readLine();
            while (line != null){
                System.out.println(line);
                line = bfr.readLine();
            }
        }catch (IOException ex){
            System.out.println("Error de lectura");
            ex.printStackTrace();
        }
        bfr.close();
        //compruebo error de terminacion del proceso
        try{
            int exitValue = p.waitFor();
            System.out.println("El proceso termina (0 = bien, 1 = mal): " + exitValue);
        }catch (InterruptedException ex){
            ex.printStackTrace();
        }
        //Leo errores que se producen al lanzar el proceso en un stream
        //los muestro linea a linea
        try {
            InputStream iserror = p.getErrorStream();
            InputStreamReader isrerror = new InputStreamReader (iserror);
            BufferedReader bfrerror = new BufferedReader (isrerror);
            //leo linea a linea para mostrarlos
            String lineerror = bfrerror.readLine();
            while (lineerror != null){
                System.out.println ("Error = " + lineerror);
                lineerror = bfrerror.readLine();
            }
        }catch (IOException ioex){
            ioex.printStackTrace();
        }
    }
    
}
