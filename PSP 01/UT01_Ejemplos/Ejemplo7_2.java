/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo7;
import java.io.*;
/**
 * @author Marina
 * La salida del ejemplo anterior, envíala al fichero ejemplo7.txt y la salida de error a errorEjemplo7.txt
 */
public class Ejemplo7_2 {
    
    public static void main(String[] args) throws IOException {
        //Proccess Builder con los argumentos
        ProcessBuilder pb = new ProcessBuilder ("CMD", "/C", "DIR");
        File fout = new File ("D:\\Curso 15-16\\psp_javas\\fout.txt"); 
        File ferror = new File ("D:\\Curso 15-16\\psp_javas\\ferror.txt");
        
        pb.redirectOutput(fout); //redirecciono la salida al fichero fout
        ProcessBuilder redirectError; 
        redirectError = pb.redirectError(ferror);
        try {
            Process p = pb.start();
            int exitValue;
            exitValue = p.waitFor();
            System.out.println ("Ejecucion proceso hijo (0 correcta): " + exitValue);
        }catch (IOException e){
            //e.printStackTrace();
            System.err.println("Error e IO");
        }catch (InterruptedException e){
            System.err.println ("El proceso hijo termina de forma incorrecta (1) y 0 si bien");
        }        
    }    
}
