/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo7;
/*** @author Marina
 * Ejemplo de uso del método enviroment () que devuelve las variables de entorno del proceso
 * También usaremos el método command () que devuelve los argumentos del proceso derinido en prueba.
 * Depués vamos a ver como a command(parametros) podemos pasarle parámetros para definir un nuevo proceso.
 */
import java.io.*;
import java.util.*;
public class Ejemplo7 {

    public static void main(String[] args) {
        ProcessBuilder pb = new ProcessBuilder(); //objeto
        Map enviroment = pb.environment(); //las variables de entorno del proceso
        
        System.out.println ("Variables de entorno = " + enviroment);
        
        //Construyo un nuevo proceso y le paso argumentos       
        pb = new ProcessBuilder ("java EscribeNombre \"marina\"");
        
        //Obtengo el nombre del proceso y los argumentos
        List lista = pb.command(); //command sin parámetros, me devuelve los argumentos pasados al proceso
        Iterator i = lista.iterator();
        System.out.println ("Agumentos del comando ");
        while (i.hasNext()){
            System.out.println (i.next());
        }
        
        //Ahora vamos a meter argumentos a command()
        pb = pb.command("CMD", "/C", "VER");
        try {
            Process p = pb.start(); //lanzamos le proceso
            InputStream is = p.getInputStream(); //recojo el stream de salida
            BufferedReader br = new BufferedReader (new InputStreamReader (is));
            String line;
            line = br.readLine();
            while (line != null) {
                System.out.println (line);
                line = br.readLine();
            }
            br.close();
            int exitValue;
            exitValue = p.waitFor();
            System.out.println ("El proceso hijo termina bien si es 0: " + exitValue);
        }catch (IOException e){
            //e.printStackTrace();
            System.err.println("Error e IO");
        }catch (InterruptedException e){
            System.err.println ("El proceso hijo termina de forma incorrecta (1) y 0 si bien");
        }
    }
    
}
