/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio5;

import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Victor
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int cantidad = 0;
        String fichero = args[1];
        try {
            cantidad = Integer.parseInt(args[0]);
        } catch (NumberFormatException n) {
            System.out.println("El primer argumento debe ser un numero)");
            System.exit(0);
        }
        try {
            FileWriter write = new FileWriter(fichero);

            for (int i = 0; i < cantidad; i++) {
                int num = (int) (Math.random() * 100);
                write.write(num+ System.lineSeparator());
            }
            write.close();
        } catch (IOException n) {
            n.printStackTrace();
        }

    }
}
