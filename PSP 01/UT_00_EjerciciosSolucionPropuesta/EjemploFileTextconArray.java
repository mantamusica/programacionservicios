/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplofiletextconarray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EjemploFileTextconArray {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String nombreFile;  //Recoge el nombre del fichero pasado
        File fichero;       //Registra el FIle del fichero pasado
        FileReader fr = null; //FileREader para la lectura del fichero.
        if (args.length == 0) {
            System.out.println ("Error. Debes pasar la ruta absoluta al fichero");
            System.exit(-1);
        }
        nombreFile = args[0];
        //Preparo el acceso al fichero
        fichero = new File(nombreFile);
        //Valido si el directorio existe
        if (!fichero.exists()) {
            System.out.println("El fichero no existe");
            System.exit (-1);
        }
        try {
            fr = new FileReader (fichero);            
        } catch (FileNotFoundException ex) {
            Logger.getLogger (EjemploFileTextconArray.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
            
        }
        try {
            int i;
            char [] arr = new char [10];
            //Array para ir leyendo los caracters del fichero en bloques de 10
            //Leo secuencialmente todos los caracteres del fichero
            i = fr.read(arr); //Leo en bolques de arr.legnth caracteres
            while (i != -1) {
                //Leo hasta el final
                for (int j=0; j<i; j++)
                    //Veo el bloque leído
                    System.out.println (arr[j]);
                i = fr.read(arr);
            }
            fr.close();     //cierro fichero
            
        } catch (IOException ex) {
            Logger.getLogger (EjemploFileTextconArray.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
