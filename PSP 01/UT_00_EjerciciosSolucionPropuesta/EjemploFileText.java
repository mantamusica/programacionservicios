/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplofiletext;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.*;

/**
 *
 * @author Marina
 */
public class EjemploFileText {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        String nombreFile; //recoge el nombre del fichero pasado
        File fichero;   //Registra el FIle del fichero pasado
        FileReader fr = null;     //FileReader para la lectura del fichero
        if (args.length ==0 ){
            System.out.println ("Error. Debe llamar al programa pasando la ruta absoluta de un fichero");
            System.exit(-1);           
        }       
        nombreFile = args [0];
        //Preparamos acceso al fichero
        fichero = new File(nombreFile);
        //validamos si el directorio existe o no
        if (!fichero.exists()){
            System.out.println("El fichero pasado no existe");
            System.exit(-1);
        }
        try {
            fr = new FileReader(fichero);            
       } catch (FileNotFoundException ex) {
            //Logger.getAnonymousLogger(EjemploFileText.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error");
            System.exit(-1);
        }
        try {
            int i;
            //Lectura secuencial de todos los caracters del fichero
            i = fr.read();
            //Leo hasta que termine el fichero
            while (i!= -1) {
                System.out.print ((char) i); //Visualizo cada caracter que leo
                i = fr.read();
            }
            fr.close(); // cierro el fichero
        } catch (IOException ex) {
            Logger.getLogger (EjemploFileText.class.getName()) . log(Level.SEVERE, null, ex);
        }
    }
    
}
