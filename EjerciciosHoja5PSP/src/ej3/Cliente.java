/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ej3;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class Cliente {

    private static int PUERTO = 8066;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ServerSocket servidor = null;
        Socket cliente = null;
        try {
            servidor = new ServerSocket(PUERTO);
            imprimeDisponible();
            while (true) {
                
                cliente = servidor.accept();
                ServidorHttp hilo=new ServidorHttp(cliente);
                hilo.start();
            }

        } catch (IOException ex) {
            Logger.getLogger(ServidorHttp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    
    private static void imprimeDisponible(){
        System.out.println ("El servidor Web escucha en el puerto " + PUERTO);
        System.out.println ("Escribe en el navegador\n http://localhost:8066 \n"
        + "para solicitar la pagina de bienvenida");
        System.out.println ("Escribe en el navegador\n http://localhost:8066/mendoza \n"
        + "para solicitar un fragmento del libro de Eduardo Mendoza \n" +
                "El secreto de la modelo extraviada");
    }
}
