/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buffercompartido;

/**
 *
 * @author Marina
 *
 */
public class Buffer {

    public boolean lleno = false;
    public boolean vacio = true;
    private int siguiente = 0; //variable para indicar la posición del buffer
    private int buffer[] = new int[6]; //buffer de 6 enteros

    /**
     * *
     * Método que evuelve el caracter consumido. Se consumen de derecha a
     * izquierda (el último caracter que se produce es el primero que se consume
     */
    public synchronized void recoger() {
        while (vacio == true) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("Se interrumpió la ejecución");
            }
        }
        //decremento en 1 la posición del buffer para simular que cojo 1 caracter
        siguiente--;
        //si la posición es ahora 0 -> era el último caracter. El buffer se vacia
        if (siguiente == 0) {
            vacio = true;
                 
        }
        //he consumido un caracter --> anoto que no está lleno
        lleno = false;
        System.out.println("Recojo número " + buffer[siguiente] + " del buffer");
        //notifico que he consumido el caracter
        notify();
    }

    /**
     * *
     * Método para poner un entero en el buffer. EL buffer se llena de 0 en
     * adelanta El número se deposita en la primera posicion libre, que será la
     * última en ser consumida *
     */
    public synchronized void poner(int num) {
        while (lleno == true) {
            try {
                wait();
            } catch (InterruptedException ex) {
                System.err.println("Se interrumpió la ejecución");
            }
        }
        //escribo el número en la última posición consumida
        buffer[siguiente] = num;
        //incremento la posición del buffer
        siguiente ++;
        //si se completó el buffer (posición 6) digo que está lleno
        if (siguiente == 6){
            lleno = true;
        }
        //ahora tengo un número para consumir, anoto buffer no vacio
        vacio = false;
        System.out.println ("Almacenado el número " + num + " en el buffer");
        notify();
    }
}