/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buffercompartido;

/**
 *
 * @author Marina
 */
public class Productor extends Thread{
    private Buffer buffer;
    private int numero;
    
    public Productor (Buffer buf){
       this.buffer =  buf;
    }
    
    @Override
    public void run(){
        int n;
        for (int i=1; i<=15; i++){
            n = (int)(Math.random()*9 + 1);
            buffer.poner(n);
            //espero un poco antes de depositar el siguiente numero
            try {
                sleep ((int)(1000*Math.random()));
            }catch (InterruptedException e){
                System.err.println ("Siesta interrumpida");
            }
        }
        
        
     
    }
}