/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buffercompartido;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Buffer buffer = new Buffer();
        Productor productor = new Productor (buffer);
        Consumidor consumidor = new Consumidor (buffer);
        productor.start();
        consumidor.start();
    }
    
}
