/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buffercompartido;

/**
 *
 * @author Marina
 */
public class Consumidor extends Thread {

    private Buffer buffer;

    public Consumidor(Buffer buf) {
        this.buffer = buf;
    }

    @Override
    public void run() {
        //consumo numeros del buffer
        for (int i = 1; i <= 15; i++) {
            buffer.recoger();
            try {
                //espera un poco antes de recoger más 
                sleep((int) (Math.random() * 1000));
            } catch (InterruptedException e) {
                System.err.println("Siesta interrumpida");
            }
          /* Para Jugar con los tiempos 
            if (buffer.vacio == true) {
                System.out.println("Buffer vacio");
                System.exit(0);
            }*/
        }

    }
}
