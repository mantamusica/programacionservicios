/*
 * 1.	Escribe una clase llamada informacionRed. 
El programa debe crear un proceso hijo que ejecute el comando ipconfig. 
Al terminar, el padre muestra por pantalla solo la línea que contiene la ip del equipo.
 */
package informacionred;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 *
 * @author Marina
 */
public class InformacionRed {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Runtime rt = Runtime.getRuntime();
        Process p = null;
        String comando = "CMD /C IPCONFIG";
        int exitValue;
        salidaFichLog ("javalog.txt");
        try{
            p = rt.exec(comando);
            InputStream is = p.getInputStream();
            BufferedReader br = new BufferedReader (
            new InputStreamReader (is));
            String readLine = br.readLine();
            while (readLine!= null){
                System.out.println (readLine);
                readLine = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            System.out.println("Error ocurrió ejecutando el comando. Descripción: "+ e.getMessage());
        }
        try {
            exitValue = p.waitFor();
            System.out.println ("0 = bien y 1 = error " + exitValue);
        }catch (InterruptedException ex){
            ex.printStackTrace();
        }
       try {
            InputStream iserror = p.getErrorStream();
            BufferedReader brerror = new BufferedReader (new InputStreamReader(iserror));
            String errorLine;
            salidaFichLog ("errorlog.txt");
            errorLine = brerror.readLine();
            while (errorLine != null){
                System.out.println ("error = " + errorLine);
                errorLine = brerror.readLine();
            }
            brerror.close();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
    public static void salidaFichLog(String fich) {
        try {
            PrintStream ps = new PrintStream(
                    new BufferedOutputStream(
                            new FileOutputStream(
                                    new File(fich), true)), true);
            System.setOut(ps);
            System.setErr(ps);
        } catch (FileNotFoundException ex) {
            System.err.println("Error. No se han podido redirigir las salidas");
            System.err.println(ex.toString());
        }
    }
}
