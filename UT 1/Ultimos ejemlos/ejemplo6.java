/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo6;
import java.io.*;
public class ejemplo6 {

   //Voy a enviar datos a la entrada stándar del programa EjemploLectura
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        Process p = null;
        String comando = "java EjemploLectura";
        try {
            p = rt.exec(comando);
            //Escribir
            OutputStream os = p.getOutputStream();
            os.write ("Hola mundo !".getBytes()); //escribo en la entrada estándar
            os.flush(); // limpio el buffer de salida
            //leer
            InputStream is = p.getInputStream();
            BufferedReader br = new BufferedReader (new InputStreamReader (is));
            String line;
            while ((line=br.readLine())!=null){
                System.out.println (line);
            }
            br.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        //compruebo error
        int exit;
        try {
            exit = p.waitFor();
            System.out.println ("Salida = " + exit);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
    
}
