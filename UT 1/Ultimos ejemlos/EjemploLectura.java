/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo6;
import java.io.*;
public class EjemploLectura {

    public static void main(String[] args) {
        InputStreamReader insr = new InputStreamReader (System.in); //System.in = Stream standar de entrada, habitualmente el teclado
        BufferedReader br = new BufferedReader (insr);
        String text;
        try {
            System.out.println ("Introduce una cadena de texto: ");
            text = br.readLine();
            System.out.println ("El texto escrito = " + text);
            insr.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
}
