/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiplesaccesos;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*** @author Marina
 *  Programa que lanza un número finito de veces la aplicación anterior, 
     * guardando el resultado en un archivo. Pasaremos por la línea de 
     * comandos el número de veces y el nombre del archivo    
     */

public class MultiplesAccesos {

    public static void main(String[] args) {
        Process nuevoProceso = null; //nuevo proceso
        String nombreArchivo ="";
        File archivo = null;
        int orden = 0;
        if (args[0]== null) {
            nombreArchivo = "prueba.txt";
        } else {
            nombreArchivo = args[0];
        }
        archivo = new File(nombreArchivo);
        try {
            for (int i=0; i<20; i++){
                nuevoProceso = Runtime.getRuntime().exec("java -jar " 
                        + "FicheroAccesoMultiple.jar " + i 
                        + "D:\\prueba\\uno.txt");
            }
        } catch (SecurityException ex){
            System.err.println("Ha ocurrido un error de Seguridad."+
                    "No se ha podido crear el proceso por falta de permisos.");
        }catch (Exception ex){
            System.err.println("Ha ocurrido un error, descripción: "+
                    ex.toString());
        }
    }
    
}
