/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multipleaccesofichero;
import java.io.*;
/** * @author Marina
 * Aplicación que crea varios procesos de la anterior. 
 * De forma que todos los procesos utilizarán el mismo fichero 
 * veremos que con el acceso concurrente sin controlar, el valor que tenemos
 * almacenado en el archivo no es el que debería ser.
 * Vamos a pasar por la linea de comandos los procesos de acceso al fichero
 * Cambiar la Aplicación "Fichero Acceso Múltiple" para que pueda recibir argumentos
 */
public class MultipleAccesoFichero {

    public static void main(String[] args) {
        Process p; //definimos el proceso
        //intentamos lanzar 20 procesos de acceso "AccesoMultipleFichero"
        try {
            //Redireccionamos el stream de salida y de error al fichero "javalog.txt"
            PrintStream ps = new PrintStream(
                             new BufferedOutputStream(new FileOutputStream(
                             new File("javalog.txt"),true)), true);
            System.setOut(ps);
            System.setErr(ps);
        } catch (FileNotFoundException ex) {
            System.err.println ("Error. No se ha redirigido la salida");
        }
        for (int i=1; i<=20; i++){
            try {
                Runtime rt = Runtime.getRuntime();
                p = rt.exec(
                        "java -jar "+
                    "FicheroAccesoMultiple.jar " + i + " nuevo.txt");
                System.out.println ("Creado el Proceso " );
            } catch (IOException ex) {
                System.out.println ("Error al crear el proceso");
            }catch (SecurityException ex){
            System.err.println("Ha ocurrido un error de Seguridad."+
                    "No se ha podido crear el proceso por falta de permisos.");
            }
        }
    }    
}
