/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package ejemplo5;
import java.io.*;
public class Ejemplo5 {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime(); 
        Process p = null;
        String comando = "CMD /C DATE";
        OutputStream os = null; //Stream de salida
        InputStream is = null; //Stream de entrada
        
        
        try {
            p = rt.exec(comando);
            //ESCRIBIR --> enviar entrada que escribo a DATE
            os = p.getOutputStream(); //recogo el stream de salida del proceso p            
            os.write("12-10-2015".getBytes()); //voy a escribir una nueva fecha           
            os.flush();  //limpio el buffer de salida
            //LEER --> Leo la salida de DATE
            is = p.getInputStream();
            BufferedReader br = new BufferedReader (new InputStreamReader (is)); 
            String line; //para leer linea a linea hasta el fin
            while ((line=br.readLine())!=null){
                System.out.println (line);
            }
            br.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        //compruebo error (0 bien y 1 mal)
        try {
            int exitVal;
            exitVal = p.waitFor();
            System.out.println ("Salida (0 bien y 1 mal)= " + exitVal);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        //hago que me muestre el stream de erores de p
        try{
            InputStream iserr= p.getErrorStream();
            BufferedReader brerr= new BufferedReader (new InputStreamReader (iserr));
            String line;
            line = brerr.readLine();
            while (line!= null){
                System.out.println("Error = " + line);
                line = brerr.readLine();
            }                
        }catch (IOException e){
            System.out.println("Error salida buffer de errores de p");
            e.printStackTrace();            
        }
    }    
}
