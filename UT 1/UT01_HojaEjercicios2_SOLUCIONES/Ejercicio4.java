/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios2;
import java.io.*;
/** * @author Marina
 * 4.	Uso de ProcessBuilder. Realiza un programa Java que ejecute varios comandos 
 * del SO dentro de un fichero BAT. Utiliza e método redirectInput() para indicar que 
 * la entrada al proceso se encuentra en un fichero, la entrada para el comando CMD es (fichero.bat). 
 * La salida del proceso se envía al fichero salida.txt y la salida de error al fichero error.txt.
 */
public class Ejercicio4 {

    public static void main(String[] args) {
        ProcessBuilder pb = new ProcessBuilder ("CMD");
        
        File fbat = new File ("instrucciones.bat");
        File fout = new File ("salida.txt");
        File ferror = new File ("error.txt");
        
        pb.redirectInput(fbat);
        pb.redirectOutput(fout);
        pb.redirectError(ferror);
        try {
            pb.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
}
