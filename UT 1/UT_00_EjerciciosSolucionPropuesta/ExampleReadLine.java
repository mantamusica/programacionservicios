/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examplereadline;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Marina
 */
public class ExampleReadLine {   

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        File fileIn; //Registro fichero entrada
        File fileOut; //Registro fichero salida
        FileReader fr; //FileReader para leer el fichero de entrada
        FileWriter fw ; //FileWriter para escribir en el fichero de salida
        BufferedReader bufr = null;  //BufferReader para usar readLine()
        BufferedWriter bufw = null ;
        if (args.length<2){
            System.out.println("Error. Debes pasar la ruta abosoluta del fichero origen y destion");
            System.exit(-1);
        }
        fileIn = new File(args[0]); //FileIn de la ruta de entrada 
        fileOut = new File(args[1]); //FileOut de la ruta de salida
        if (!fileIn.exists()){
            System.out.println("Error, el fichero de entrada no exsite");
            System.exit(-1);
        }
        try{
            fr = new FileReader(fileIn);           
            bufr = new BufferedReader (fr);            
            //bufr = new BufferedReader (new FileReader (fileIn));
            fw = new FileWriter (fileOut);
            bufw = new BufferedWriter (fw);
        }catch (FileNotFoundException ex){
            Logger.getLogger(ExampleReadLine.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        try {
            String line= bufr.readLine();
            
            while (line!=null){   
                
                bufw.write(line);   
                bufw.newLine();
                line =bufr.readLine();  
            }     
            bufr.close();
            bufw.close();
        }catch(IOException ex)
        {
            System.out.println("Error de escritura del fichero");                       
        }  
        
    }    
}
