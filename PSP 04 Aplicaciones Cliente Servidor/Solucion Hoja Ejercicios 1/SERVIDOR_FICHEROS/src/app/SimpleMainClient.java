/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

/**
 *
 * @author Rocio
 */
public class SimpleMainClient {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final String HOST = "localhost";
        final int PORT = 18000;
        Socket sc = null;
        DataInputStream in;
        DataOutputStream out;
        String str;
        BufferedReader br;
       
        try {
            sc = new Socket(HOST, PORT); // Creamos la conexión
            br = new BufferedReader(new InputStreamReader(System.in));
            in = new DataInputStream(sc.getInputStream());
            out = new DataOutputStream(sc.getOutputStream());

            System.out.println(in.readUTF()); // Recibimos menu
            out.writeUTF(br.readLine()); // Elegimos opción
            
            while (sc.isConnected()) {
                str = in.readUTF(); // Recibimos validación
                if (str.matches("\\d")) {
                    switch (str) {
                        case "1":
                            System.out.println(in.readUTF());
                            break;
                        case "2":
                            System.out.println(in.readUTF());
                            out.writeUTF(br.readLine());
                            System.out.println(in.readUTF());
                            break;
                        case "3":
                            System.out.println("Deshabilitando conexión");
                            break;
                        default:
                            System.out.println("No deberías estar aquí");
                            break;
                    }
                } else {
                    System.out.println(str);
                    out.writeUTF(br.readLine());
                }
            }

        } catch (SocketException sce) {
            System.out.println("SocketException");
        } catch (IOException ioe) {
            System.out.println("IOException");
        }
    }
}
