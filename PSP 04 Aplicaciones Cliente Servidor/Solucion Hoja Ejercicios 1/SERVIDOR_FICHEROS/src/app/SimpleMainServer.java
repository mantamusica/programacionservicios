/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rocio
 */
public class SimpleMainServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final int PORT = 18000, DELAY = 10;
        ServerSocket svr; // Habilita conexión servidor
        Socket sc; // Guarda conexión/direccion cliente

        try {

            svr = new ServerSocket(PORT);
            while (true) {
                System.out.println("Servidor espera petición conexión del cliente");
                sc = svr.accept();
                System.out.println("Conexión entrante aceptada");
                sc.setSoLinger(true, DELAY); // Retrasamos futuro cierre en 10s

                new ConexClient(sc).start();
            }
        } catch (SocketException se) {
            Logger.getLogger(SimpleMainClient.class.getName()).log(Level.SEVERE, null, se);
        } catch (IOException ex) {
            Logger.getLogger(SimpleMainClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static class ConexClient extends Thread {

        final String MENU = "\nSelecciona una opción (1, 2 ó 3)\n"
                + "1.- Listar subdirectorios y archivos del directorio actual\n"
                + "2.- Descargar el contenido de un fichero\n"
                + "3.- Salir\n";
        DataInputStream in;
        DataOutputStream out;
        Socket sc;
        String opt = null, dir = null, str = null, tmp;
        File f;
        File[] ls;

        public ConexClient(Socket sc) {
            try {
                in = new DataInputStream(sc.getInputStream());
                out = new DataOutputStream(sc.getOutputStream());
                this.sc = sc;
            } catch (IOException ex) {
                Logger.getLogger(SimpleMainServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void run() {
            try {
                while (true) {
                    out.writeUTF(MENU);
                    do {
                        opt = in.readUTF();
                        if (!opt.matches("[1-3]{1}")) {
                            out.writeUTF("OPCIÓN INCORRECTA\n" + MENU);
                        } else {
                            out.writeUTF(opt);
                        }
                    } while (!opt.matches("[1-3]{1}"));
                    switch (opt) {
                        case "1":

                            f = new File("files" + File.separator);
                            ls = f.listFiles();
                            for (int i = 0; i < ls.length; i++) {
                                if (i != 0) {
                                    dir += "\n" + ls[i].getName();
                                } else {
                                    dir += ls[i].getName();
                                }
                            }
                            out.writeUTF(dir);
                            break;
                        case "2":
                            
                            out.writeUTF("Introduzca el nombre del archivo completo");
                            f = new File("files" + File.separator + in.readUTF());
                            if (!f.exists()) {
                                out.writeUTF("El archivo no existe");
                                break;
                            } else {
                                FileReader fr = new FileReader(f);
                                // Consejo de NetBeans a saber porqué
                                try (BufferedReader br = new BufferedReader(fr)) {
                                    while ((tmp = br.readLine()) != null) {
                                        str += tmp + "\n";
                                    }
                                    out.writeUTF(str);
                                }
                            }
                            str = null;
                            break;
                        case "3":
                            System.out.println("Opcion 3");
                            sc.close();
                            break;
                        default:
                            System.out.println("No deberías estar aquí");
                            break;
                    }
                }

            } catch (IOException ioe) {
            }
        }
    }
}
