/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 *
 * @author USUARIO
 */
public class Cliente {

    //parámetros de conexión
    static final String HOST = "localhost";
    static final int PUERTO = 18000;

    //comandos
    static final String DIR = "dir";
    static final String GET = "get";
    static final String EXIT = "exit";
    //marca de fin escritura
    static final String EOF = "EOF";

    /**
     * El cliente crea los flujos y muestra las opciones que puede escoger el
     * cliente liberando de dicha tarea al servidor.
     */
    public Cliente() {

        try {
            Socket cliente = new Socket(HOST, PUERTO);
            // Creo los flujos de entrada y salida
            DataInputStream entrada = new DataInputStream(cliente.getInputStream());
            DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
            int opcion = 0; //variable inicializada para ver la opcion escribe cliente            

            while (opcion != 3) {
                System.out.println("\nMenú de opciones: ");
                System.out.println("\t 1 - Ver el contenido del directorio.");
                System.out.println("\t 2 - Ver el contenido de un archivo.");
                System.out.println("\t 3 - Salir.");
                System.out.println("Escoge la opción que quieras (1-2-3): \n");
                Scanner teclado = new Scanner(System.in); //para leer del teclado
                opcion = teclado.nextInt();
                switch (opcion) {
                    case 1:
                        orden_Dir(entrada, salida);
                        break;
                    case 2:
                        orden_Get(entrada, salida);
                        break;
                    case 3:
                        salida.writeUTF(EXIT);
                        break;
                    default:
                        System.out.println("Escoge una opción válida (1-2-3");
                        break;
                }

            }
            salida.writeUTF(EXIT);           
            salida.close();
            entrada.close();
            cliente.close();
        } catch (IOException ex) {
            System.out.println("Error de entrada salida" + ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     *
     * @param entrada
     * @param salida
     */
    private void orden_Dir(DataInputStream entrada, DataOutputStream salida) {
        String respuesta = new String();
        try {
            salida.writeUTF(DIR); //escribo comando en stream para el servidor            
            while (!respuesta.equals(EOF)) {
                respuesta = entrada.readUTF(); //para recoger que me manda el servidor
                System.out.println(respuesta);
            }            
        } catch (IOException ex) {
            System.err.println("Error de escritura en el canal" + ex.toString());
        }
    }

    private void orden_Get(DataInputStream entrada, DataOutputStream salida) {
        try {
            System.out.println("Escribe el nombre del archivo: ");
            Scanner teclado = new Scanner(System.in); //para leer del teclado
            String archivo = teclado.next();
            salida.writeUTF(GET);
            salida.writeUTF (archivo);
            String respuesta = new String(); //para recoger la respuesta del servidor
            do {
                respuesta = entrada.readUTF();
                if (!respuesta.equals(EOF)) {
                    System.out.println(respuesta);
                }
            } while (!respuesta.equals(EOF));
        } catch (IOException ex) {
            System.err.println("Error al escibir y leer en el canal");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        new Cliente();
    }

}
