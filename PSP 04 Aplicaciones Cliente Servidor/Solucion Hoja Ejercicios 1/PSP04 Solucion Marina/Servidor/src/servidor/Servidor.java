/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.*;
import java.net.*;

/**
 *
 * @author solas
 */
class Servidor extends Thread {
    //conexion
    private Socket cliente = null;
    private static final int PUERTO = 18000;

    //estados para elegir que hacer
    private static final int INICIO = 1;
    private static final int DIRECTORIO = 2;
    private static final int ARCHIVO = 3;
    private static final int SALIR = -1;

    //comandos
    private static final String DIR = "dir";
    private static final String GET = "get";
    private static final String EXIT = "exit";

    //marca fin de transmision que me creo 
    private static final String EOF = "EOF";

    //constructor del servidor
    public Servidor(Socket sc) {
        cliente = sc;
    }

    public static void main(String[] arg) {
        ServerSocket Servidor = null;
        try {
            Servidor = new ServerSocket(PUERTO);

            System.out.println("Servidor conectado");
            while (true) {
                // Se conecta un cliente
                Socket skCliente = Servidor.accept();
                System.out.println("Cliente conectado");
                new Servidor(skCliente).start(); //arranco hilo para cliente
            }

        } catch (IOException ex) {
            System.err.println("Error conexion cliente.  " + ex.toString());
        } finally { //Cierra el socket servidor

            try {
                Servidor.close();
            } catch (IOException ex) {
                System.err.println("Error cerrando socket: " + ex.toString());
            }

        }
    }

    /**
     * run() lo que hace el servidor para un hilo cliente conectado Defino los
     * flujos y establezco los siguientes estados: ver el comando ejecutar dir
     * ejecutar ver archivo salir
     */
    @Override
    public void run() {
        DataInputStream entrada = null;
        DataOutputStream salida = null;

        //Variables de estado y del comando.
        int estado = INICIO;
        String comando = new String();

        try {
            entrada = new DataInputStream(cliente.getInputStream());
            salida = new DataOutputStream(cliente.getOutputStream());           
            
            while (estado != SALIR && cliente.isConnected()) {
                switch (estado) {
                    case INICIO:
                        try {                            
                            comando = entrada.readUTF();
                            //en funcion de la respuesta decido que hacer
                            if (comando.equals(DIR)) { //mandan ver directorio
                                estado = DIRECTORIO;
                                System.out.println("Estado = " + estado);                               
                            } else if (comando.equals(GET)) { //mandan ver archivo
                                estado = ARCHIVO;
                                System.out.println("Estado = " + estado);
                            } else if (comando.equals(EXIT)) { //mandan salir
                                estado = SALIR;
                            } else { //opción no válida
                                salida.writeUTF("Escoge una opción válida.");
                            }
                        } catch (IOException ex) {
                            System.err.println("Error lectura del comando");                            
                        }
                        break;
                    case DIRECTORIO:
                        ejecutaDir(salida);
                        estado = INICIO;
                        break;
                    case ARCHIVO:                  
                        //leo del flujo de entrada el nombre que me envia el cliente
                        String fich = entrada.readUTF();                       
                        System.out.println ("Fichero que el cliente quiere: "+ fich);
                        ejecutaGet_Archivo(entrada, salida, fich);                        
                        estado = INICIO;
                        break;
                    case SALIR:
                        System.out.println("Adios");
                        break;
                    default:
                        break;
                }
            }

        } catch (Exception e) {
            System.err.println("Error en el proceso principal: " + e.getMessage());
        } finally {

            try {
                salida.close();
                entrada.close();
            } catch (IOException ex) {
                System.err.println("Error al cerrar los flujos. " + ex.toString());
            }
            System.out.println("Adios");
        }
    }

    /**
     * Ver directorio recibe el flujo de salida para escribir el resultado de
     * ejecutar el proceso DIR en la consola. Otra alternativa sería leer el
     * resultado del servidor linea a linea y escribirlo en el stream de salida.
     */
    /**
     *
     * @param salida
     * @throws IOException
     */
    private void ejecutaDir(DataOutputStream salida) throws IOException {

        try {
            System.out.println("estoy en VerDirectorio");
            String comando = "CMD /C DIR";    //comando a ejecutar en la consola     
            Runtime rt = Runtime.getRuntime();
            Process p = rt.exec(comando);
            InputStream is = p.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String linea;
            linea = br.readLine();
            while (linea != null) {
                salida.writeUTF(linea);
                linea = br.readLine();
            }
            salida.writeUTF(EOF);
            br.close();
        } catch (IOException ex) {
            System.out.println("Error al ejecutar comando DIR");
            salida.writeUTF("Error al ejecutar comando dir");
        }
    }

    /**
     * @param entrada
     * @param salida
     * @param fichero
     * @throws IOException
     */
    private void ejecutaGet_Archivo(DataInputStream entrada, DataOutputStream salida, String fichero) throws IOException {
        try {
            String linea;         
            System.out.println("Cliente solicita ver: " + fichero);
            // Leo el fichero linea a linea y la escribo en el flujos salida
            FileInputStream fstream = new FileInputStream(fichero);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            linea = br.readLine(); 
            while (linea != null) {
                salida.writeUTF(linea); 
                linea = br.readLine();
            }
            in.close();
            salida.writeUTF("EOF"); //escribo marca de fin
        } catch (IOException ex) {
            System.out.println("Error IO del fichero");
            salida.writeUTF("Error, el fichero no existe");
            salida.writeUTF("EOF"); 
        } catch (Exception ex) {
            salida.writeUTF("Error: " + ex.toString());
            System.out.println("Error: " + ex.toString());

        }
    }
}
