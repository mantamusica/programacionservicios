
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Fernando
 */
public class Cliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        final int PUERTO = 18000;
        Socket s = new Socket("localhost", PUERTO);
        String frase="";
        OutputStream salidaOS = s.getOutputStream();
        DataOutputStream flujoDataOut = new DataOutputStream(salidaOS);
        InputStream entradaOS = s.getInputStream();
        DataInputStream flujoDataIn = new DataInputStream(entradaOS);
        do {
            System.out.println(flujoDataIn.readUTF());
            int numero = Integer.parseInt(teclado.readLine());
            flujoDataOut.writeInt(numero);
            switch (numero) {
                case 1:
                    frase = flujoDataIn.readUTF();
                    System.out.println(frase);
                    break;
                case 2:
                    System.out.println("Introduzca el nombre del fichero");
                    flujoDataOut.writeUTF(teclado.readLine());
                    while(!(frase=flujoDataIn.readUTF()).equals("99")){
                        System.out.println(frase);
                    }
                    break;
                case 3:
                    frase = flujoDataIn.readUTF();
                    System.out.println(frase);
                    break;
            }
        } while (!frase.equals("ADIOS"));
    }

}
