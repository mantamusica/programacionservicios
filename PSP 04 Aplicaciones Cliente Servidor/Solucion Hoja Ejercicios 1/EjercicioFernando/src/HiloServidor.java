
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Fernando
 */
public class HiloServidor extends Thread {

    Socket socket = null;
    OutputStream salidaOS = null;
    InputStream entradaOS = null;

    public HiloServidor(Socket s) {
        this.socket = s;
    }

    @Override
    public void run() {
        try {
            int numero = 0;
            salidaOS = socket.getOutputStream();
            DataOutputStream flujoDataOut = new DataOutputStream(salidaOS);
            entradaOS = socket.getInputStream();
            DataInputStream flujoDataIn = new DataInputStream(entradaOS);
            do {
                flujoDataOut.writeUTF("1- Ver el contenido del directorio actual\n"
                        + "2- Ver el contenido de un fichero de dicho directorio. Si el fichero existe, el servidor enviará su contenido al cliente y este podrá verlo por pantalla. Si el fichero no existe, el servidor avisará al cliente con un mensaje.\n"
                        + "3- Salir.");
                numero = flujoDataIn.readInt();

                switch (numero) {
                    case 1:
                        VerDirectorio(flujoDataOut);
                        break;
                    case 2:
                        VerArchivo(flujoDataIn,flujoDataOut);
                        break;
                    case 3:
                        flujoDataOut.writeUTF("ADIOS");
                        break;
                }
            } while (numero != 3);
        } catch (IOException ex) {
            Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void VerDirectorio(DataOutputStream flujoDataOut) {
        try {
          
            String frase = "";
            File f = new File(".");
            String[] ficheros = f.list();
            for (int i = 0; i < ficheros.length; i++) {
                frase = frase + ficheros[i] + "\n";
            }
            flujoDataOut.writeUTF(frase);
        } catch (IOException ex) {
            Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void VerArchivo(DataInputStream flujoDataIn, DataOutputStream flujoDataOut) {
        try {
            
            String nombre = flujoDataIn.readUTF();
            File f = new File(nombre);
            if (f.exists()) {
                BufferedReader lee = new BufferedReader(new FileReader(f));
                String linea="";
                while((linea=lee.readLine())!=null){
                    flujoDataOut.writeUTF(linea);
                }
                flujoDataOut.writeUTF("99");
            } else {
                flujoDataOut.writeUTF("El fichero no existe");
                flujoDataOut.writeUTF("99");
            }
        } catch (IOException ex) {
            Logger.getLogger(HiloServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
