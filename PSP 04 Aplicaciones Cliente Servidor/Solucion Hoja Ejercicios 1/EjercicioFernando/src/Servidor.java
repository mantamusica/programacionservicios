
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Fernando
 */
public class Servidor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        final int PUERTO =18000;
        ServerSocket servidor = new ServerSocket(PUERTO);
        while(true){
            Socket socket=servidor.accept();
            HiloServidor hilo = new HiloServidor(socket);
            hilo.start();
        }
    }
    
}
