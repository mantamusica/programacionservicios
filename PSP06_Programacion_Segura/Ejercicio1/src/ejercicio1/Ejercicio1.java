/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author msr
 */
public class Ejercicio1 {

    private String login;
    private String fichero;
    static final Logger logger = Logger.getLogger("MyLog");

    public Ejercicio1() {
        IniciarLog();

        LeerLogin();
        System.out.println("Usuario " + login + " se ha conectado");
        logger.log(Level.INFO, "Se ha conectado el usuario = {0}", login);

        LeerFichero();
        System.out.println("El usuario quiere ver el fichero: " + fichero);
        logger.log(Level.INFO, "El usuario quiere ver el fichero = {0}", fichero);

        VerFichero(fichero);
    }

    private void VerFichero(String fichero) {
        try {
            String d = System.getProperty("user.home", "not specified");
            fichero = d + "\\" + fichero;
            FileInputStream fis = new FileInputStream(fichero);
            DataInputStream dis = new DataInputStream(fis);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(dis));
            String linea;
            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
            }
            dis.close();
        } catch (FileNotFoundException ex) {
            System.err.println("No se ha encontrado el fichero");
        } catch (IOException ex) {
            System.err.println("Error IO al leer del fichero");
        }
    }

    private void LeerFichero() {
        fichero = new String();

        Pattern pattern = null;
        Matcher matcher = null;

        //leo del teclado
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            do {
                System.out.println("Escribe el nombre del fichero.extension: ");
                fichero = br.readLine();
                //patron para el nombre del fichero
                pattern = Pattern.compile("[a-z]{1,8}.[a-z]{3}");
                matcher = pattern.matcher(fichero);
            } while (!matcher.matches());
        } catch (IOException ex) {
            System.err.println("Error IO al leer del fichero");
        }
    }

    private void LeerLogin() {
        login = new String();
        Pattern pattern = null;
        Matcher matcher = null;

        //leo del teclado
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            do {
                System.out.println("Nombre de usuario en minúsculas (no más de 8 caracteres): ");
                login = br.readLine();
                //patron para el nombre del fichero
                pattern = Pattern.compile("[a-z]{1,8}");
                matcher = pattern.matcher(login);
            } while (!matcher.matches());

        } catch (IOException ex) {
            System.err.println ("Error IO en LeerLogin()");
            Logger.getLogger(Ejercicio1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void IniciarLog() {
        FileHandler fh;
        try {
            // Configuro el logger y establezco el formato
            fh = new FileHandler("./log_actividad.log", true);
            logger.addHandler(fh);
            logger.setLevel(Level.ALL);
            logger.setUseParentHandlers(false);

            SimpleFormatter formatter = new SimpleFormatter();

            fh.setFormatter(formatter);

        } catch (SecurityException | IOException e) {
            System.err.println ("Error al iniciar el log" + e.toString());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.setSecurityManager(new SecurityManager());
        Ejercicio1 ejercicio = new Ejercicio1();
    }

}
