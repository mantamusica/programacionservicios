/*
 * Vamos a ver algunas de las propiedades que obtenemos con el método
 * System.getProperty().
 * Ponemos todas las propiedades en un array.
 */
package ejemplo0_props;

/**
 *
 * @author msr
 */
public class Ejemplo0_props {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //propiedades del sistema
        String propiedades[] = {
            "java.class.path", "java.home", "java.vendor", "java.version",
            "os.name", "os.version", "user.dir", "user.home", "user.name"
        };
        //vemos las propiedades
        for (int i = 0; i < propiedades.length; i++) {
            try {
                String prop = System.getProperty(propiedades[i]);
                System.out.println("\n* " + propiedades[i] + " = " + prop);
            } catch (Exception e) {
                System.err.println("Caught exception " + e.toString());
            }
        }

    }

}
