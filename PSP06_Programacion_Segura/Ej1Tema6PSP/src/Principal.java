
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author usuario
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        File log = new File("log.txt");
        BufferedWriter bfr = new BufferedWriter(new FileWriter(log,true));

        System.setProperty("java.security.policy", "./fichero_pol.policy");
        //System.setProperty("java.security.policy", "D:\\usuario\\Desktop\\pruebas\\fichero_pol.policy");
        System.setSecurityManager(new SecurityManager());
        // TODO code application logic here
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        File fich = null;

        boolean salir = false;

        String nom = "";
        while (!salir) {
            System.out.println("Dame el nombre del usuario");
            nom = teclado.readLine();
            if (!(nom.matches("[a-z]{8}"))) {
                System.out.println("Nombre incorrecto.");
            } else {
                salir = true;
                bfr.write(getCurrentTimeUsingDate()+"\t"+"Entró " + nom);
                bfr.newLine();
            }
        }

        String nomFich;
        salir = false;
        while (!salir) {
            System.out.println("Dame el nombre del fichero");
            nomFich = teclado.readLine();
            if (!(nomFich.matches(".{1,8}[.]+[a-z]{3}"))) {
                System.out.println("Nombre incorrecto.");
            } else {
                fich = new File(System.getProperty("user.home") + "\\" + nomFich);
                bfr.write(getCurrentTimeUsingDate()+"\t"+"Fichero a leer: " + nomFich);
                bfr.newLine();
                salir = true;
            }
        }
        
        if (fich.exists()) {
            BufferedReader fichR = new BufferedReader(new FileReader(fich));
            String pal = fichR.readLine();
            while (pal != null) {
                System.out.println(pal);
                pal = fichR.readLine();
            }
            bfr.write(getCurrentTimeUsingDate()+"\t"+"Se leyó el fichero " + fich.getName());
            bfr.newLine();
        } else {
            System.out.println("No se encuentra el fichero");
            bfr.write(getCurrentTimeUsingDate()+"\t"+"No se leyó el fichero " + fich.getName());
            bfr.newLine();
        }
        
        
        bfr.write(getCurrentTimeUsingDate()+"\t"+"El usuario " + nom + " terminó.");
        bfr.newLine();
        bfr.close();

    }
    
    public static String getCurrentTimeUsingDate() {
        Date date = new Date();
        String strDateFormat = "hh:mm:ss a";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate= dateFormat.format(date);
        return formattedDate;
    }

}
