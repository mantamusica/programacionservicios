/*
 * Hilo que incrementa el valor de la variable compartida cuenta.
 */
package semaforoejemplo1;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Marina
 */
public class HiloIncrementa extends Thread {

    private Contador contador;
    Semaphore sem;

    public HiloIncrementa(String nombre, Contador c, Semaphore s) {
        this.contador = c;
        this.sem = s;
        this.setName(nombre);
    }

    @Override
    public void run() {
        //intentamos coger el recurso con el semaforo
        try {
            sem.acquire();
            contador.SubeCuenta();
            sem.release();//suelto recurso, libero semaforo
        } catch (InterruptedException e) {
            System.err.println("Error. Hilo interrumpido\n" + e.toString());
        }
    }
}
