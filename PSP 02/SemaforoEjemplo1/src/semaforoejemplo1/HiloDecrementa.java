/*
 * Hilo que decrementa la cuenta de la variable compartida
 */
package semaforoejemplo1;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Marina
 */
public class HiloDecrementa extends Thread{
    private Contador contador;
    private Semaphore sem;
    
    public HiloDecrementa (String nombre, Contador c, Semaphore s){
        this.setName(nombre); //identificador del hilo
        this.contador = c;
        this.sem= s;
    }
    @Override
    public void run(){
        //cogemos el recurso con el semaforo y bajo el valor de cuenta
        try {
            sem.acquire();
            contador.BajaCuenta(); //baja en 1 el valor de la cuenta
            sem.release(); //libero el semaforo
        }catch (InterruptedException e){
            System.err.println ("Error. Hilo interrumpido\n" + e.toString());
        }
    }
}
