/*
 * Clase con la variable compartida contador
Incluye los métodos para subir y para bajar el valor del contador.
 */
package semaforoejemplo1;

/**
 *
 * @author Marina
 */
public class Contador {
    private int cuenta; //variable con el valor de la cuenta
    public Contador (int valorInicial) {
        this.cuenta = valorInicial; //valor inicial de la cuenta
    }
    /*
    Método que incrementa la variable cuenta
    Sube el valor de la cuenta en 1
    */
    public void SubeCuenta (){
        System.out.println (Thread.currentThread().getName() + " SUBE cuenta");
        cuenta++; 
        System.out.println ("Cuenta = " + cuenta);
    }
    /*
    Me´todo que decrementa la variable cuenta 
    Baja el valor de la variable en 1
    */
    public void BajaCuenta(){
         System.out.println (Thread.currentThread().getName() + " BAJA cuenta");
        cuenta--; 
        System.out.println ("Cuenta = " + cuenta);
    }
}
