/*
 * EJEMPLO SEMAFORO: 
Variable compartida por dos hilos.
Un hilo incrementa la cuenta y otro la decrementa

 */
package semaforoejemplo1;

import java.util.concurrent.Semaphore;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //probamos a subir en 100 el contador y bajarlo en 50
        int cuentaInicial = 20;
        Contador cuenta = new Contador (cuentaInicial);
        Semaphore sema; 
        sema = new Semaphore(1); //semaforo
        //icnremento en 100 la cuenta
        for (int i=1; i<=100; i++){
            HiloIncrementa hi;
            hi = new HiloIncrementa ("Hilo Incr " +i, cuenta, sema);
            hi.start();
        }
        //decremento en 50 la cuenta
        for (int i=1; i<=50; i++){
            HiloDecrementa hd;
            hd = new HiloDecrementa ("Hilo Decr " + i, cuenta, sema);
            hd.start();
        }
    }
    
}
