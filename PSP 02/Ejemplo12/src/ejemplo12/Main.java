/*
 * Se inician 15 hilos con prioridades diferentes, 5 con prioridad máxima, 
5 con prioridad normal y 5 con prioridad mínima. 
Al ejecutar el programa los hilos con prioridad más alta tienden a finalizar antes. 
 */
package ejemplo12;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int contador = 5; //numero de hilos a iniciar
        
        //Vectores con distintas prioridades
        //5 con prioridad min, 5 con normal y 5 con maxima
        Thread[] hiloMinP = new Thread[contador];
        Thread[] hiloNorP = new Thread[contador];
        Thread[] hiloMaxP = new Thread[contador];
        
        //creo los 5 hilos de minima prioridad
        for (int i=0; i<contador; i++)
            hiloMinP[i] = new Hilo(Thread.MIN_PRIORITY);
        //creo los hilos de prioridad normal
        for (int i=0; i<contador; i++)
            hiloNorP[i] = new Hilo(Thread.NORM_PRIORITY);
        //creo los hilos de maxima prioridad
        for (int i=0; i<contador; i++)
            hiloMaxP[i] = new Hilo(Thread.MAX_PRIORITY);
        
        System.out.println ("Hilos en proceso.\n" +
                "Los de mayor prioridad se ejecutan antes");
        //iniciamos los hilos
        for (int i=0; i<contador; i++){
            hiloMinP[i].start();
            hiloNorP[i].start();
            hiloMaxP[i].start();
        }
        
    }
    
}
