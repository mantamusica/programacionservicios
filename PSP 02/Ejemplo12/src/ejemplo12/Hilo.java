/*
 * hilo cuya tarea es llenar un vector con 20000 caracteres.
 Uso del método yield().
 */
package ejemplo12;

/**
 *
 * @author Marina
 */
public class Hilo extends Thread {

    //constructor por defecto

    public Hilo() {
        //hereda la prioridad del hilo padre
    }

    /**
     * **********Constructor personalizado******************
     * @param prioridad
     */
    public Hilo(int prioridad) {
        //establcer la prioridad indicada
        this.setPriority(prioridad);
    }

    /**
     * ***** Ejecutar una tarea pesada**********
     */
    @Override
    public void run() {
        String cadena = "";
        //añadir 20000 caracteres a una cadena vacía
        for (int i = 1; i <= 20000; i++) {
            cadena += "A";
            //voy a sugerir al planificador Java que pueda seleccionar otro hilo
            yield();
        }
        System.out.println ("CADENA = " +cadena);
        //imprimo la prioridad del hilo
        System.out.println("Hilo de prioridad " + this.getPriority()
                + " ha terminado");
    }
}
