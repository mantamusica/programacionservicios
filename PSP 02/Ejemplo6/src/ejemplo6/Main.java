/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo6;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HiloSecundario hilo = new HiloSecundario(); //creo hilo
        //Obtengo el estado del hilo y si está vivo
        System.out.println ("Hilo Secundario NUEVO. Estado = " +
                hilo.getState() + " ¿Vivo? = " +
                hilo.isAlive());
        hilo.start(); //Iniciamos el hilo que pasa a EJECUTABLE
        
        System.out.println ("Hilo Secundario INICIADO. Estado = " +
                hilo.getState() + " ¿Vivo? = " +
                hilo.isAlive());
        try {
            hilo.join(); //espero que el hilo muera
        }catch (InterruptedException e){
            System.out.println ("Hilo Secundario MUERTO. Estado = " +
                    hilo.getState() + " ¿Vivo o muerto? " +
                    hilo.isAlive());
        }
    }
    
}
