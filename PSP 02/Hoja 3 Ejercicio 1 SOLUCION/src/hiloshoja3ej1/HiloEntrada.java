/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hiloshoja3ej1;

/**
 *
 * @author kike
 */
public class HiloEntrada extends Thread {

    Garaje gara;

    public HiloEntrada(Garaje gara) {
        this.gara = gara;
    }

    @Override
    public void run() {
        synchronized(gara) {
            gara.incrementar();
        }
    }

}
