/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplo1;

/**
 *
 * @author Marina
 */
public class AccesoAhiloPrincipal {

    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        System.out.println ("Hola mundo!.");
        // Obtengo el hilo donde se está ejecutando este método       
        Thread hilo = Thread.currentThread();
        //Imprimo el nombre del hilo en la salida estándar. Uso getName()
        System.out.println ("El hilo que ejecuta el método main() "
            +"de este programa se llama '" + hilo.getName() + "'\n");
    }  
    
}
