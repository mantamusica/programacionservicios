/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo10;

/**
 *
 * @author Marina
 */
public class Museo {
    //clase que simula el museo con las entradas y salidas
    private int cuenta; //para contar las entradas y salidas del museo
    public Museo (){
        cuenta = 0;
    }
    public void IncrementaCuenta (){
        System.out.println ("Hilo " +Thread.currentThread().getName() +
                " ENTRA en el museo");
        //incremento la cuenta 
        cuenta ++;
        System.out.println ("Personas en el museo = " + cuenta);
        
    }
    
    public void DecrementaCuenta (){
        System.out.println ("Hilo " + Thread.currentThread().getName() +
                " SALE del mueso");
        cuenta --;
        System.out.println ("Personas en el museo = " + cuenta);
    }
}

