/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo10;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Museo museo = new Museo(); //creo objeto museo
        
        //hago entrar a 10 hilos
        for (int i =1; i<=10; i++){
            Entra dentro;
            dentro = new Entra(" " +i, museo);
            dentro.start();
        }
        //hago salir a 5 hilos
        for (int i=1; i<=5; i++){
            Sale fuera;
            fuera = new Sale (" " +i, museo);
            fuera.start();
        }
    }
    
}
