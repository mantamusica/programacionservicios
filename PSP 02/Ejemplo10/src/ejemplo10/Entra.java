/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo10;

/**
 *
 * @author Marina
 */
public class Entra extends Thread{
    private Museo museo;
    
    Entra (String nombre, Museo m){
        this.setName(nombre);
        this.museo = m;
    }
    
    @Override
    public void run(){
        //tengo que usar segmento sincronizado
        synchronized (museo){
            museo.IncrementaCuenta();
        }
    }
}
