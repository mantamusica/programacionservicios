/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo3;

/**
 *
 * @author Marina
 */
public class HiloEjemplo3 extends Thread {
    //Propiedades, constructor y métodos.
    private int contador; //contador de cada hilo
    private int hilo; 
    //constructor
    public HiloEjemplo3 (int hilo){
        this.hilo=hilo;
        System.out.println ("Creado el hilo:" +hilo);
    }
    //método run
    @Override
    public void run (){
        contador = 0;
        while (contador <5){            
            System.out.println("Hilo en run: " + hilo + " - contador = " +contador);
            contador ++;
        }
    }
    
}
