package PaquetePrincipal;
/**
 *
 * @author IMCG
 */
/**
 * hilo que realiza una cuenta de 1 a 20, cuyo valor representa en el marcador
 * a medida que aumenta*/
public class Hilo_Auxiliar extends Thread {
    //clase que hereda de Thread
    //JPanel_Marcador mi_marcador;
    //marcador local
    boolean duerme;
    JFrame_Ventana jf;
    //variable para controlar si dormimos o no al hilo
    public Hilo_Auxiliar(boolean d,JFrame_Ventana mijf) {
        //constuctor del hilo
        duerme = d;
        jf=mijf;
        //almacena el marcador recibido
    }

    @Override
    public void run() {
        //código del hilo
        jf.setMarcador(0);
        //anula la cuenta
        //solicita el repintado para borrar el marcador
        if (duerme) {
            for (int i = 1; i <=20; i++) {
                //incremental la cuenta
                jf.setMarcador(i);
                //solicita el repintado
                try {
                    this.sleep(100);
                    //duerme el hilo actual durante 1 décima de segundo, para
                    //que la petición de repintado del marcador sea atendida
                } catch (InterruptedException ex) {
                }
            }
        } else {
            for (int i = 1; i <= 20; i++) {
                //incremental la cuenta
                jf.setMarcador(i);
                //solicita el repintado
            }
        }
    }
}
