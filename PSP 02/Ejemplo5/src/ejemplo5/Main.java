/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo5;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Thread h1 = new HiloThread ("PSP");
        Thread h2;
        h2 = new HiloThread();
        Thread h3 = new Thread (new HiloRunnable());
        
        //hacemos correr a los tres hilos
        h1.start();
        h2.start();
        h3.start();
    }
    
}
