/*
 *Museo sincronizado con monitores
La variable compartida es "cantidad" en Museo
 */
package ejemplo9;

/**
 *
 * @author Marina
 */
public class Museo {
     //clase que simula el museo con las entradas y salidas
    private int cuenta; //para contar las entradas y salidas del museo
    public Museo (){
        cuenta = 0;
    }
    /*Poner método synchronized significa que:
    - Hemos creado un monitor asociado al objeto
    - Solo un hilo puede ejecutar el método de ese objeto cada vez
    - El resto de hilos permanece bloqueado y en espera
    - Cuando el hilo finaliza la ejecución del método, los hilos en espera
    se desbloquearán y el planificador de Java seleccionará al siguiente.
    */
    public synchronized void IncrementaCuenta (){
        System.out.println ("Hilo " +Thread.currentThread().getName() +
                " ENTRA en el museo");
        //incremento la cuenta 
        cuenta ++;
        System.out.println ("Personas en el museo = " + cuenta);
        
    }
    
    public synchronized void DecrementaCuenta (){
        System.out.println ("Hilo " + Thread.currentThread().getName() +
                " SALE del mueso");
        cuenta --;
        System.out.println ("Personas en el museo = " + cuenta);
    }
}
