/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HiloSumaPares hilosp = new HiloSumaPares(); //hilo suma pares
        HiloSumaImpares hilosi = new HiloSumaImpares(); //hilo suma impares
        hilosp.start(); //invoco la ejecución del hilo suma pares
        hilosi.start(); //Invoco la ejecución del hilo suma impares
    }
    
}
