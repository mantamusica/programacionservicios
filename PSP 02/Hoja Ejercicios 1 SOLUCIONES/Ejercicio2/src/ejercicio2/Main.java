/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        
        //compruebo argumentos
        if (args.length != 1){
            System.err.println ("Error de ejecución. Indica el nombre del fichero");
            System.exit (-1);
        }
        
        Thread hilo = new CuentaLineasFich (args[0]);        
        hilo.start();
        //probar a eliminar el join()
        hilo.join(); //Para que padre espere a que hijo termine
        System.out.println ("Terminada ejecución hijos. El padre se despide");
        
    }
    
}
