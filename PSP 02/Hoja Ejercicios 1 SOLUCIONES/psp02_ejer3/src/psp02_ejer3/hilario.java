/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psp02_ejer3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dgf
 */
public class hilario extends Thread {

    private String nombre;
    private int proceso;

    public hilario(String nombre, int proceso) {
        this.nombre = nombre;
        this.proceso = proceso;
    }

    @Override
    public void run() {
        BufferedReader file = null;
        int contador = 0;
        try {
            file = new BufferedReader(new FileReader(nombre));

            while (file.readLine() != null) {
                contador++;
            }
            System.out.println("El hilo "+proceso+" conto "+contador+" lineas en el fichero "+nombre);

        } catch (FileNotFoundException ex) {
            System.out.println("No existe el fichero");
        } catch (IOException ex) {
            Logger.getLogger(hilario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
