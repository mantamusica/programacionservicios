/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psp02_ejer3;

/**
 *
 * @author dgf
 */
public class Psp02_ejer3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length <1) {
            System.out.println("No se ha pasado el numero correcto de argumentos");
            System.exit(-1);
        }

        for (int i = 0; i < args.length; i++) {
            hilario hilo = new hilario(args[i],i);
            hilo.start();
        }
    }

}
