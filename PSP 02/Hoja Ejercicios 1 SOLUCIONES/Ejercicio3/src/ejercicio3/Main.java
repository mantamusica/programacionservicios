/*
 * Modifica el programa anterior para que lea el número de líneas de todos los 
 ficheros pasados como argumentos. El programa creará un array de hilos con 
 tantos elementos como ficheros pasados como argumentos. El programa principal 
 esperará a que termine la ejecución de todos los hilos.
 */
package ejercicio3;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //compruebo que se pasan ficheros como argumentos
        if (args.length == 0) {
            System.err.println("Debes pasar el nombre de los ficheros");
            System.exit(-1);
        }

        Thread[] arrayHilos = new Thread[args.length];
        int nlineas = 0;
        for (int i = 0; i < args.length; i++) {
            arrayHilos[i] = new CuentaLineas(args[i]);
            arrayHilos[i].start();

            try {
                arrayHilos[i].join();
            } catch (InterruptedException ex) {
                System.err.println("Error en hilo " + i);
                System.err.println(ex.toString());
            }

        }
        System.out.println ("El padre ha terminado la ejecucion de los hilos");
    }
}

