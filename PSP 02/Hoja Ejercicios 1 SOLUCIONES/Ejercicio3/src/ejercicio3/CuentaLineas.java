/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Marina
 */
public class CuentaLineas extends Thread {

    String fichero;

    public CuentaLineas(String fichero) {
        this.fichero = fichero;
    }

   

    @Override
    public void run() {
        int nlineas = 0;

        try {
            BufferedReader bfr = new BufferedReader(
                    new FileReader(
                            new File(fichero)));
            while (bfr.readLine() != null) {
                nlineas ++;
            }
            System.out.println ("Número de lineas en fichero " +
                    fichero + " = " + nlineas);
        } catch (FileNotFoundException ex) {
            System.err.println("Error. No hay fichero");
            System.err.println(ex.toString());
        } catch (IOException ex) {
            System.err.println ("Error de lectura del fichero");
            System.err.println (ex.toString());
        }

    }
}
