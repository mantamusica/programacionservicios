El tocador de Se�oras (Eduardo Mendoza).
Yo me limit� a suspirar y a mirar hacia otro lado. 
La familia que regentaba la pizzer�a, compuesta por la se�ora Margarita, el se�or Calzone y su hijito Cuatroquesos, eran a mis ojos el paradigma de la felicidad, un ideal al que yo no cre�a poder aspirar, pero cuya visi�n me colmaba a la vez de alborozo y melancol�a. 
A lo largo de los �ltimos a�os me hab�a convertido en su mejor cliente y ellos correspond�an a mi asiduidad con su simpat�a y su cari�o. 
En la pizzer�a sent�a, siquiera de modo vicario, el calor del hogar que jam�s conoc�. 