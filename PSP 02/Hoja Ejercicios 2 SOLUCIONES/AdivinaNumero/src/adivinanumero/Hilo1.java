/*
 * Hilo 1. Genera numero aleatotorio entre 0 y 10
lo compara con el introducido por teclado, si acierta, termina
Si no acierta, sigue pidiendo numeros por teclado.
Para controlar el acierto o no, usa una bandera.
 */
package adivinanumero;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Marina
 */
public class Hilo1 extends Thread{
    
    public boolean bandera = true; //inicializo la variable booleana
    int aleatorio, leido; //para el numero generado y el del teclado
    public Hilo1 () {} //constructor
    
    @Override
    public void run(){
        //genero numero aleatorio enre 1 y 10
        aleatorio = (int) (Math.random()*11); 
        //leo del teclado
        BufferedReader br = new BufferedReader (
            new InputStreamReader (System.in));
        try {
            System.out.println ("Adivina el número entre 0 y 10");
            while (bandera){ //pida numero hasta cambie bandera
                System.out.println ("Escribe un número ");
                leido = Integer.parseInt(br.readLine());
                if (aleatorio == leido){ //si coincide valor
                    System.out.println ("GANASTE");
                    bandera = false; //Gané. Dejo de pedir números.
                }
                else
                    //si no he acertado muestro mensaje pide volver a intentarlo
                    System.out.println ("Puedes volver a intentarlo");
            }
        }catch (IOException | NumberFormatException e){
            System.err.println ("Error de entrada salida o de formato");
            System.err.println (e.toString());
        }
    }
    
}
