/*
 * Ejecuta al Hilo1. Controla el fin de la ejecución cuando o bien se 
ha acertado el numero (valor de la bandera es true o bien deja pasar 10 veces
de 1 milisegundo cada una
 */
package adivinanumero;

/**3
 *
 * @author Marina
 */
public class Hilo2 extends Thread{
    public Hilo2 (){}//constructor
    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run(){
        Hilo1 h1= new Hilo1();  //objeto hilo1
        h1.start(); //empieza la ejecución del hilo 1
        for (int i=0; i< 10; i++){
            try {
                sleep(1000);
            }catch (InterruptedException e){
                System.err.println ("Ejecucion interrumpida");
                System.err.println (e.toString());
            }
            if (h1.bandera==false){
                i=10;
            }
        }
        if (h1.bandera)
             System.out.println ("PERDISTE EL JUEGO");
        
        System.exit(0);
        h1.bandera= false;
    }
    
}
