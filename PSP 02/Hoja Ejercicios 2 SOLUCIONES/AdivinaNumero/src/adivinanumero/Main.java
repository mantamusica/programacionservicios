/*
 Programa en java que simule un juego de adivinar un número entre uno y 10. 
Para ello definimos dos hilos. 
El primero de ellos tiene una variable booleana y nos va pidiendo el número 
por teclado. Si lo adivina, cambia el valor de la variable booleana, y saca un
mensaje de que ha ganado el juego.
El segundo hilo llama a ejecutar el primero y se duerme durante 10 veces,
un segundo cada vez. Para cada vez, pregunta si la variable booleana del 
primer hilo ha sido acertado o no. Si ha sido acertado, termina la ejecución 
del hilo. Si no acierta el número después de ese tiempo, saca un mensaje 
diciendo que se acabo el tiempo de juego y que ha perdido. El programa principal
solo hace una llamada al segundo hilo.

 */
package adivinanumero;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Hilo2 h2 = new Hilo2();
        h2.start();
    }
    
}
