/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ej1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 *
 * @author usuario
 */
public class Hilo1 extends Thread{
    int num;
    boolean acertado=false;
    
    @Override
    public void run(){
        Random rd=new Random();
        num=rd.nextInt(10);
        
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        do{
            System.out.println("Dame un número:");
            try{            
                int numLeido=Integer.parseInt(in.readLine());
                if(num==numLeido){
                    acertado=true;
                }
            }catch(NumberFormatException e){
                System.out.println("Se esperaba un número");
            }catch(IOException e){
                System.out.println("Se produjo un error en la lectura");
            }
        }while(!acertado);      
    }
}
