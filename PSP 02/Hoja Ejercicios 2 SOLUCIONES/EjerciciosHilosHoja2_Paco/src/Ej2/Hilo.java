/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ej2;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class Hilo extends Thread{
    int cont;

    public Hilo(int cont){
        this.cont=cont;
    }
    
    public void run(){
        if(cont>0){        
            System.out.println("Hilo "+cont+" comienza");
            cont--;
            Hilo hilo=new Hilo(cont);
            hilo.start();
                try {
                    hilo.join();
                    for(int i=0;i<10;i++){
                        Random rd=new Random();
                        long num=(rd.nextInt(5)+1)*100;
                        System.out.println("(Espera "+num+" ms)");
                        hilo.sleep(num);
                        System.out.println("Estoy en el hilo "+(cont+1));
                    }
//                    System.out.println("Se ha acabado el hilo "+(cont+1));
                    System.out.println("Mi hilo podría haber muerto");
                } catch (InterruptedException ex) {
                }
            }
    }
}
