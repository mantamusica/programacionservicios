/*
 Aplicación java que crea un hilo, este a su vez cree otro , y así hasta 5 hilos. 
 Cada hilo tiene que presentar su nombre indicando que comienza y crear a su hilo hijo. 
 A continuación, cada hilo debe esperar a que su hijo termine y presentar de 
 nuevo su nombre diez veces indicando que se está procesando y esperando un tiempo 
 aleatorio entre 100 y 600 ms entre cada presentación.  
 Por último, deberá presentar un mensaje indicando su nombre y que ha terminado
 
El programa principal solo arranca al primer hijo
*/
package ejercicio2;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Hilo hijo = new Hilo(5);
        hijo.start(); //arranco el primer hilo
        try {
            hijo.join();
        } catch (InterruptedException e) {
            System.err.println("Interrupción de la ejecución del hilo");
            System.err.println(e.toString());
        }
    }

}
