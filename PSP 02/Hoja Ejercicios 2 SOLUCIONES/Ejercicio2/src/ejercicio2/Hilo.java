
/*
 el hilo 5 crea al hilo 4, el 4 al 3, el 3 al 2 y el 2 al 1. 
Cada hilo espera a que su hijo termine. 
El 1 muestra su nombre diez veces y termina. 
Entre vez y vez espera un numero aleatorio entre 100 y 600 ms
Como ha terminado, el 2 que lo estaba bloqueado se desbloquea y 
muestra su nombre diez veces y termina y así sucesivamente con todos los hilos
 */
package ejercicio2;

/**
 *
 * @author Marina
 */
public class Hilo extends Thread{
    int numHijo;
    Hilo hiloCreado;
    
    Hilo (int HijoNum){ //constructor
        //recordar que los constructores no se heredan...
        //this alude a todo el objeto
        //super() alude solo a la parte heredada.
        super();
        this.numHijo = HijoNum; 
    }
    
    @Override
    public void run(){
        System.out.println ("Creo el hijo número: " + numHijo);
        /*
        Cada hilo arranca al anterior. 
        Hilo 5 crea al 4. El hilo 4 crea al 3, ...*/
        //
        if (numHijo >1){
            Hilo hijo = new Hilo (numHijo-1); //construyo el hilo
            hijo.start(); //arranco el hilo hijo
            try{ //espera a que termine la ejecución del hijo
                hijo.join();
            }catch (InterruptedException e){
                System.err.println ("Interrupción de la ejecución del hilo");
                System.err.println (e.toString());
            }
        }
        //cada hilo escribe su nombre 10 veces
        for (int i=1; i<=10; i++){
            System.out.println (" - soy el hijo " + numHijo);
            //doy tiempo aleatorio entre 100 y 600 ms para la presentacion
            try {
                Thread.sleep((long)(Math.random()*600));//error comprobar
            }catch (InterruptedException e2){
                System.err.println ("Interrupción de la ejecución del hilo");
                System.err.println (e2.toString());
            }
        }
        System.out.println ("Hijo " + numHijo + " terminó");
    }
    
}
