/*
 * Contar personas que entran a un museo con dos puertas Entrada y Salida
-Unos hilos simularán la Entrada y otros la Salida del museo
- Todos los hilos comparten un recurso, la variable cuenta, que se incrementa o
decrementa en uno según sea Entrada o Salida
-Suponemos que incialmente hay 0 personas en el museo
 */
package ejemplo8;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Museo museo = new Museo(); //creo objeto museo
        
        //hago entrar a 10 hilos
        for (int i =1; i<=10; i++){
            Entra dentro;
            dentro = new Entra(" " +i, museo);
            dentro.start();
        }
        //hago salir a 5 hilos
        for (int i=1; i<=5; i++){
            Sale fuera;
            fuera = new Sale (" " +i, museo);
            fuera.start();
        }
    }
    
}
