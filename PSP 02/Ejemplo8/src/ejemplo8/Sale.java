/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo8;

/**
 *
 * @author Marina
 */
public class Sale extends Thread{
    private Museo museo;
    
    Sale (String nombre, Museo m){
        this.setName(nombre);
        this.museo=m;
    }
    @Override
    public void run(){
        museo.DecrementaCuenta();
    }
}
