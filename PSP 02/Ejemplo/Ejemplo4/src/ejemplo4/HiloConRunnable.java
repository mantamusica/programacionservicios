/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo4;

/**
 *
 * @author Marina
 */
public class HiloConRunnable implements Runnable{
    @Override
    public void run() {
        //método run del codigo asociado al hilo
        System.out.println ("Hola! soy run desde el hilo creado con Runnable");
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Construyo objeto runnable
        HiloConRunnable mirunnable = new HiloConRunnable();
        //Creo hilo asociado a ese runnable
        Thread hilo = new Thread(mirunnable); 
        hilo.start(); //invoco método start para empeazar el hilo
    }
    
}
