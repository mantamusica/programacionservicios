/*
 * Problema Lectores/Escritores resuelto con wait() y notify().
* Sobre los mismos datos tenemos:
   - 3 escritores escribiendo
   - 10 lectores leyendo
  Sincronizamos un semáforo.
 */
package ejemplo13;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Semaforo sem = new Semaforo();
        // 5 lectores y 2 escritores
        for (int i=1; i<=5; i++){
            new LectorHilo ("Lector "+i, sem).start();
        }
        for (int i=1; i<=5; i++){
            new EscritorHilo ("Escritor "+i, sem).start();
        } 
    }
    
}
