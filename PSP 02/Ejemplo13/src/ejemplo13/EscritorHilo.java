/*
 *Hilo que escribe datos.
 */
package ejemplo13;

/**
 *
 * @author Marina
 */
public class EscritorHilo extends Thread {
    
    private Semaforo semaforo; //construyo el semaforo
    
    //constructor: recibe el semaforo y el nombre
    public EscritorHilo (String nombre, Semaforo sem){
        this.setName(nombre);
        this.semaforo = sem;
    }
    @Override
    public void run(){
        System.out.println ("Intenta escribir = " + getName());
        semaforo.Escribir();
        //el hilo ha escrito
        /*7
        compruebo el funcionamiento dando un tiempo de siesta. 
        */
        try {
            //duermo el hilo un tiempo aleatorio antes de comunicar el fin 
            //de lectura. Doy ocasión a que los demás hilos hagan intentos
            //fallidos de lectura/escritura
            sleep ((int) (Math.random())*50);
        }catch(InterruptedException e){
            System.err.println ("Error");
            System.err.println (e.toString());
        }
        semaforo.FinEscritura();
    }
}
