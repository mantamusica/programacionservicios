/*
 Clase semaforo encargada de controlar el acceso de lectore y escritores a los datos
 Incluye métodos para actualizar el semaforo:
 - lector ha terminado y escritor puede escirbir
 - escritor ha terminado y lector puede leer
 */
package ejemplo13;

/**
 *
 * @author Marina
 */
public class Semaforo {

    /**
     * Con tres variables vamos a indicar: si los datos están libres. Si hay
     * lectores leyendo. Puede entrar otro lector. No un escritor Si hay
     * escritores escribiendo. Nadie puede entrar
     */
    public final static int libre = 0; //Constante expresa datos Es libre
    public final static int conLector = 1; //Constante expresa hay lectores
    public final static int conEscritor = 2; //Constant expresa hay escritores

    /**
     * *Inicializo el semaforo. Datos libres y sin lectores***
     */
    private int estado = libre;
    private int lectores = 0;

    /**
     * ***Metodo acceso a lectura de datos metodo sincronizado = solo un hilo
     * lo usa cada vez****
     */
    public synchronized void Leer() {
        String nombreHilo = Thread.currentThread().getName();
        if (estado == libre) {
            System.out.println("Datos: " + estado + " " + lectores + ""
                    + nombreHilo + " entra a leer.");
            //cambio el estado. Ya hay lector
            estado = conLector;
        } else if (estado != conLector) {
            //si ni libre ni con lectores. Entonces puede escribir
            while (estado == conEscritor) {
                try {
                    System.out.println("Datos: " + estado + " lectores " + lectores
                            + " " + nombreHilo + " quiere leer. ESPERA");
                    wait(); //el hilo que quiere leer en estado de espera
                } catch (InterruptedException ex) {
                    System.err.println("Error");
                    System.err.println(ex.toString());
                }
            }
            System.out.println("Datos: " + estado + " lectores " + lectores + ""
                    + nombreHilo + " entra a leer.");
            estado = conLector;
        } else { //estado con lectores
            System.out.println("Datos: " + estado + " lectores " + lectores + ""
                    + nombreHilo + " entra a leer.");

        }
        lectores++;
        System.out.println("Datos: " + estado + " lectores " + lectores
                + nombreHilo + " están leyendo.");
    }

    /**
     * ***Metodo acceso a escritura de datos metodo sincronizado = solo un hilo
     * lo usa cada vez****
     */
    public synchronized void Escribir() {
        String nombreHilo = Thread.currentThread().getName();
        if (estado == libre) {
            System.out.println("Datos: " + estado + " lectores " + lectores
                    + nombreHilo + " entra a escribir.");
            estado = conEscritor;
        } else {
            //si no estálibre espera
            while (estado != libre) {
                try {
                    System.out.println("Datos: " + estado + " lectores= " + lectores
                            + " " + nombreHilo + " quiere escribir. ESPERA");
                    wait(); //el hilo que quiere escribir en estado de espera
                } catch (InterruptedException ex) {
                    System.err.println("Error");
                    System.err.println(ex.toString());
                }
            }
            System.out.println("Datos: " + estado + "  lectores " + lectores
                    + " " + nombreHilo + " entra a escribir.");
            estado = conEscritor;
            System.out.println("Datos: " + estado + " lectores " + lectores
                    + nombreHilo + " está escribiendo.");
        }
    }

    /**
     * ****** Metodo que invoca un hilo escritor cuando termina de escribir
     * para actualizar el estado del semaforo y, si acaso, notificar el cambio a
     * los hilos en espera. DOS hilos NO pueden ejecutar estas instrucciones a
     * la vez ********
     */
    public synchronized void FinEscritura() {
        estado = libre;
        System.out.println(Thread.currentThread().getName()
                + " ha terminado de escribir");
        notify(); //notifico el fin a los hilos en espera
    }

    /**
     * ****** Metodo para indicar fin de lectura *********
     */
    public synchronized void FinLectura() {

        System.out.println(Thread.currentThread().getName()
                + " ha terminado de leer");
        lectores--;
        if (lectores == 0) {
            estado = libre;
            notify(); //notifico el fin a los hilos en espera
        }

    }

}
