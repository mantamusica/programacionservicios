/*
 * Hilo que lee
 */
package ejemplo13;

/**
 *
 * @author Marina
 */
public class LectorHilo extends Thread{
    private Semaforo semaforo;
    
    public LectorHilo (String nombre, Semaforo sem){
        this.setName(nombre);
        this.semaforo= sem;
    }
    @Override
    public void run (){
        System.out.println ("Intenta leer " + getName());
        semaforo.Leer();
        try {
            //duermo el hilo un tiempo aleatorio antes de comunicar el fin 
            //de lectura. Doy ocasión a que los demás hilos hagan intentos
            //fallidos de lectura/escritura
            sleep ((int) (Math.random())*50);
        }catch(InterruptedException e){
            System.err.println ("Error");
            System.err.println (e.toString());
        }
        semaforo.FinLectura();
    }
}
