/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProductorConsumidor;

/**
 *
 * @author Marina
 */
public class Consumidor extends Thread{
    private Recipiente recip; //referencia al recipiente
    private int numCon; //identificador del consumidor
    public Consumidor (Recipiente r, int c){
        this.recip= r;
        this.numCon=c;
    }
    /*
    run() invoca a get() para vaciar recipiente y se bloque unos segundos
    */
    @Override
    public void run(){
        int contRecuperado = 0;
        for (int i=1; i<= 10; i++){
            contRecuperado = recip.get();
            System.out.println ("Consumidor " + numCon + " coge " + contRecuperado);
            try{
                sleep((int)(Math.random()*1000));                
            }catch (InterruptedException e) {
                System.err.println("El hilo se interrumpió");
            }
        }
    }
}
