/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProductorConsumidor;

/**
 *
 * @author Marina
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Recipiente recipiente = new Recipiente();
        Productor p = new Productor (recipiente, 1);
        Consumidor c = new Consumidor (recipiente, 1);
        p.start();
        c.start();
    }
    
}
