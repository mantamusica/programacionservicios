/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProductorConsumidor;

/**
 *
 * @author Marina
 */
public class Productor extends Thread{
    private Recipiente recip; //almacena la referencia al recipiente
    private int num; //identificdor del hilo productor
    
    public Productor (Recipiente r, int numPro){
        this.recip = r;
        this.num= numPro;
    }
    /*
    run() invoca a put y espera un tiempo determinado antes de volver a llenar
    */
    @Override
    public void run(){
        for (int i=1; i<=10; i++){
            
            recip.put(i);
            System.out.println ("Productor " + num + " pone dato " +i);
            try {
                sleep((int)(Math.random()*1000));
            }catch (InterruptedException e) {
                System.err.println("El hilo se interrumpió");
            }
        }
    }
}
