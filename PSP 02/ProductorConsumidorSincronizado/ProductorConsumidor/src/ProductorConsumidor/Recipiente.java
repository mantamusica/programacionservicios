/*
 *  MONITOR. 
 Tenemos un atributo "recipiente" donde vamos a almacenar un entero.
 Tenemos dos métodos:
 Llenar recipiente 
 Vaciar recipiente
 Si el recipiente no está vacio, esperamos a que un hilo consumidor lo vacie.
 Una vez vaciado, debe notificar que está vacío, por si algun productor espera

 Si no se ha llenado el recipiente, el consumidor espera a que esté lleno. 
 Una vez lleno, se notifica, por si algún consumidor espera
 */
package ProductorConsumidor;

/**
 *
 * @author Marina
 */
public class Recipiente {

    private int contenido; //almacena el contenido del recipiente
    private boolean lleno = false; //si recipiente leno o no
    /*
     Método get, lo usan los consumidores. Si no lleno, se bloquea hasta lo esté
     Cuando recipiente lleno, sale del bloqueo, indica que coge el contenido
     y notifica que recipiente vacio a los procesos en espara para llenarlo
     */

    public synchronized int get() {
        while (lleno ==false) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("El hilo se interrumpió");
            }
        }
        lleno = false;
        notifyAll();
        return contenido;
    }
    /*
     Método put() lo usan los productores.
     Si recipiente lleno, espera notificacion de vacio.
    Cuando recibe notificación de vacio, llena el recipiente y notifica lleno
     */
    public synchronized void put(int valor){
        while (lleno ==true){
            try{
               wait();
            }catch (InterruptedException e) {
                System.err.println("El hilo se interrumpió");
            }
        }
        contenido = valor;
        lleno = true;
        notifyAll();
    }
}
/*
wait()= hilo se bloquea a la espera y el control del monitor queda libre para 
que lo use otro hilo
sleep() = el hilo se bloquea y no libera el monitor--> 
    impide a otro hilo la ejecucion
*/
