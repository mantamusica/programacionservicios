/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheroaccesomultiple;
import java.io.*;
/* @author Marina
Aplicación que accede a un fichero que contiene un valor entero, lee el valor, 
lo incrementa en 1 y escribe el valor actualizado en el mismo fichero
 */
public class FicheroAccesoMultiple {

    public static void main(String[] args) {
        int orden = 0;  //para tener el número de orden
        int dato = 0; //para recoger el valor leido del archivo
        String nombrearchivo;
        nombrearchivo = "";
        String SistemaOperativo; //ver si tengo Win o Linux
        File archivo;        
        FileReader lee = null;    //para leer del flujo del archivo
        BufferedReader br = null; 
        FileWriter escribe = null; //para escribir en el flujo del archivo
        PrintWriter pw;     //para escribir en el archvio
        
        //Como en la aplicación 2 voy a lanzar a esta varias veces en la forma
        //java -jar FicheroAccesoMultiple.jar" + i + "nuevo.txt
        //tengo que contemplar la posibilidad del paso por argumentos del fichero
       //Comprobamos si estamos recibiendo argumentos en la línea de comandos
      if (args.length > 0)
          orden = Integer.parseInt(args[0]);
          //Número de orden de creación de este proceso
        try {
        //fichero log donde escribir y mostrar al usuario lo que hace el programa
        //redirecciono la salida estándar al fichero. Uso de System.setOut (PrintStream)
        //también voy a redireccionar al mismo fichero la salida de error                    
            
            PrintStream ps = new PrintStream(
                             new BufferedOutputStream(new FileOutputStream(
                             new File("javalog.txt"),true)), true);
            System.setOut(ps); //Redirecciono o asigno salida estandar al fichero
            System.setErr(ps); //Redirecciono o asingo errores al fichero
        }catch (Exception e){
            System.err.println ("P" + orden + "Error. No se ha redirigido la salida. Orden: ");
        }
        //Voya a identificar si estoy en Windows o Linux, para pasar la ruta del archivo
        SistemaOperativo = System.getProperty("os.name");
        if (SistemaOperativo.toUpperCase().contains("WIN")){
            //cuando uso la aplicación 2 recibo el nombre del fichero como arg
            if (args.length>1)//me han pasado el fichero como argumento
                nombrearchivo = args[1];               
            else
                nombrearchivo = "accesomultiple.txt";
        }
        else { //Linux
            if (args.length>1)//me han pasado el fichero como argumento
                nombrearchivo = args[1];               
            else
                nombrearchivo = "/home/user/accesomultiple.txt";
        }
        //Si el archivo sobre el que leo y escribo no existe, lo creo        
        archivo = new File (nombrearchivo);
        if (!archivo.exists()){
            try {  
                archivo.createNewFile();
                escribe = new FileWriter (nombrearchivo);
                pw = new PrintWriter (escribe);
                pw.println (String.valueOf(0)); //escribo 0 en el archivo
                //Voy a escribir en mi javalog el mensaje 
                System.out.println ("Proceso " + orden + " - Creando archivo");
            }catch (Exception e) {
                System.err.println ("Proceso " + orden + " - ERROR al crear fichero");
            }finally {
                //independientemente del comportamiento de los errores, voy a cerrar flujos
                try {
                    if (escribe != null)
                    escribe.close();
                }catch (Exception ex){
                    System.err.println ("Proceso " + orden + " - ERROR al cerrar archivo");
                    System.exit(-1); //finalizo si hay error
                }                    
            }      
        }
        try {
            //Leer del archivo
            lee = new FileReader(archivo);
            br = new BufferedReader (lee);
            String line;
            line = br.readLine();
            dato = Integer.parseInt(line);
            System.out.println ("Proceso " + orden + " - Dato leido del archivo= " + dato);
        } catch (FileNotFoundException ex) {
            System.err.println ("Error. Archivo no encontrado");
        } catch (IOException ex) {
            System.err.println ("Error de lectura del archivo");
        }finally {
            //independientemente de si hay error o no me aseguro de que el flujo se cierra
            try {
                if (lee != null)
                    lee.close();
            }catch (Exception e){
                System.err.println ("Proceso " + orden + " Error al cerrar el archivo.");
                System.exit (-1); //salgo con error
            }
        }  
        //Incremento en "1" el dato
        dato++;
        //Escribo el dato en el fichero
        try{
            escribe = new FileWriter (nombrearchivo);            
            pw = new PrintWriter (escribe);
            pw.println(String.valueOf(dato)); //escribo el dato incrementado
            System.out.println ("Proceso " + orden +  " - Dato escrito en el archivo= " + dato);
        }catch (IOException e){
            System.err.println ("Error de escritura del archivo");
        }finally{
            //nos aseguramos de que esté cerrado el flujo
            try {
                if (escribe != null)
                    escribe.close();
            }catch (Exception e){
                System.err.println ("Proceso " + orden + " Error al cerrar el archivo.");
                System.exit (-1); //salgo con error
            }
        }
    }
}
